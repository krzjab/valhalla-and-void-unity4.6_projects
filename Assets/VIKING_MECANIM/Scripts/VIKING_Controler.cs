﻿using UnityEngine;
using System.Collections;

// Require these components when using this script
[RequireComponent(typeof (Animator))]
[RequireComponent(typeof (CapsuleCollider))]
[RequireComponent(typeof (Rigidbody))]
[RequireComponent(typeof (AudioSource))]

public class VIKING_Controler : MonoBehaviour {

	private Animator anim;							// a reference to the animator on the character
	public AnimatorStateInfo currentBaseLayer;
	public AnimatorStateInfo currentBaseStateBattleLayer;	// a reference to the current state of the animator, used for battle state layer
	public AnimatorStateInfo currentBaseStateAttackLayer;	// a reference to the current state of the animator, used for attack state layer
	public AnimatorStateInfo currentBaseStateBattleMoveLayer;	// a reference to the current state of the animator, used for battle move layer
	private CapsuleCollider col;					// a reference to the capsule collider of the character

	public float maxRotation = 2f;					//kontroluje sprędkość rotacji
	public float maxSpeed = 0.0f;

	public float animSpeed = 1.5f;				// a public setting for overall animator animation speed
	
	static int idleState = Animator.StringToHash("Base Layer.Idle");	
	static int BattleState = Animator.StringToHash("BattleStance.BattleStanceIdle");
	static int DefenceToBattle = Animator.StringToHash("BattleStance.DefenceToBattlestance");
	static int a1_after = Animator.StringToHash("Attack.Attack1_after");	
	static int a2_after = Animator.StringToHash("Attack.Attack2_after");	
	static int a3_after = Animator.StringToHash("Attack.Attack3_after");	
	static int none = Animator.StringToHash("Attack.New State");
	static int DefenceState = Animator.StringToHash("Attack.Defence_stance");
	// State for counting damage on shieldBash colision and mecanim control
	public int ShieldBash = Animator.StringToHash("Attack.ShieldBash");
	// State for couting damage on sword collision
	public int attack1 = Animator.StringToHash("Attack.Attack1_attacking");	
	public int attack2 = Animator.StringToHash("Attack.Attack2_attacking");	
	public int attack3 = Animator.StringToHash("Attack.Attack3_attacking");	
	// State for walking in battle/nonBattle modes
	static int walking = Animator.StringToHash("Base Layer.walking_going");
	static int walkingBattle = Animator.StringToHash("BattleMove.BattleStanceWalk_walking");
	// walking sound
	public AudioClip walkingSound;

	// enable key working
	public bool KeysON;
	public bool battleStatesON;

	// horizontal and vertical float parameters
	public float h;
	public float v;

	// Use this for initialization
	void Start () {
		// initialising reference variables
		anim = GetComponent<Animator>();					  
		col = GetComponent<CapsuleCollider>();	
		battleStatesON = true;
		anim.SetBool("Battle", true);
		KeysON = true;
	}
	void DisableBattleStates()
	{
		battleStatesON = false;
		anim.SetBool ("Battle", false);	
	}
	void EnableBattleStates()
	{
		battleStatesON = true;
	}
	void EnableControls()
	{
		KeysON = true;
	}
	void DisableControls()
	{
		KeysON = false;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		// public AnimatorStateInfo GetCurrentAnimatorStateInfo(int layerIndex);
		currentBaseLayer = anim.GetCurrentAnimatorStateInfo (0);
		currentBaseStateBattleLayer = anim.GetCurrentAnimatorStateInfo (1);	// set our currentState variable to the current state of the Base Layer (1) of animation
		currentBaseStateBattleMoveLayer = anim.GetCurrentAnimatorStateInfo(2);
		currentBaseStateAttackLayer = anim.GetCurrentAnimatorStateInfo (3);	// set our currentState variable to the current state of the Base Layer (3) of animation
		if (KeysON == true) {
						h = Input.GetAxis ("Horizontal");				// setup h variable as our horizontal input axis
						v = Input.GetAxis ("Vertical");				// setup v variables as our vertical input axis
						anim.SetFloat ("Speed", v);							// set our animator's float parameter 'Speed' equal to the vertical input axis				
						anim.SetFloat ("Direction", h); 						// set our animator's float parameter 'Direction' equal to the horizontal input axis		
						anim.speed = animSpeed;								// set the speed of our animator to the public variable 'animSpeed'
						
	
						//Rotacja postaci i animacja rotacji
						float Rotation = Input.GetAxis ("Horizontal");
						gameObject.rigidbody.angularVelocity = new Vector3 (rigidbody.angularVelocity.x, Rotation * maxRotation, rigidbody.angularVelocity.z); 
						//Move forward
						WalkingForward(v);

						// Battlestance state
						BattleStateOn ();
						BattleStateReady ();
						// Attacks---Fighting
						BattleActions ();
						
				}
		WalkingSound ();
	}
	void BattleStateOn()
	{
		if (Input.GetButtonDown("BattleStateOnOff") && battleStatesON==true) 
		{
			if ( anim.GetBool("Battle")==false /*&& anim.GetBool("ReadyForBattle")==false*/ )
			{
				anim.SetBool("Battle", true);
			}
			else
			{
				//anim.SetBool ("ReadyForBattle", false);	
				anim.SetBool ("Battle", false);	
				
			}
		}	
	}
	void BattleStateReady()
	{
		if (currentBaseStateBattleLayer.nameHash == BattleState) {
			anim.SetBool ("ReadyForBattle", true);
		} else {
			anim.SetBool ("ReadyForBattle", false);
		}
	}
	IEnumerator Attack1()
	{
		anim.SetBool ("Attack1", true);
		yield return new WaitForSeconds (0.1f);
		anim.SetBool ("Attack1", false);
	}
	IEnumerator Attack2()
	{
		anim.SetBool ("Attack2", true);
		yield return new WaitForSeconds (0.1f);
		anim.SetBool ("Attack2", false);
	}
	IEnumerator Attack3()
	{
		anim.SetBool ("Attack3", true);
		yield return new WaitForSeconds (0.1f);
		anim.SetBool ("Attack3", false);
	}
	IEnumerator ShieldBash_attack()
	{
		anim.SetBool("ShieldHit", true);
		yield return new WaitForSeconds (0.1f);
		anim.SetBool("ShieldHit", false);
	}
	void BattleActions()
	{
		if (anim.GetBool ("Battle") == true && anim.GetBool ("ReadyForBattle") == true) 
		{
			if( (Input.GetButtonDown("Fire1")) && ((currentBaseStateAttackLayer.nameHash == none) || (currentBaseStateAttackLayer.nameHash == DefenceToBattle) ) )
			{
				StartCoroutine("Attack1");
				GameObject.Find ("SoundEffectPlayer").SendMessage("FSound"); // emit fighting sound fx
				GameObject.Find ("SoundEffectPlayer").SendMessage("SwordSwing1"); // emit fighting sound fx
			}
			if( (Input.GetButtonDown("Fire1")) && (currentBaseStateAttackLayer.nameHash == a1_after) ) 
			{
				StartCoroutine("Attack2");
				GameObject.Find ("SoundEffectPlayer").SendMessage("FSound"); // emit fighting sound fx
				GameObject.Find ("SoundEffectPlayer").SendMessage("SwordSwing2"); // emit fighting sound fx
			}
			if( (Input.GetButtonDown("Fire1")) && (currentBaseStateAttackLayer.nameHash == a2_after) ) 
			{
				StartCoroutine("Attack3");
				GameObject.Find ("SoundEffectPlayer").SendMessage("FSound"); // emit fighting sound fx
				GameObject.Find ("SoundEffectPlayer").SendMessage("SwordSwing3"); // emit fighting sound fx
			}
			if( (Input.GetButton("Fire2")) && ((currentBaseStateAttackLayer.nameHash == a2_after ) || (currentBaseStateAttackLayer.nameHash == a1_after ) || (currentBaseStateAttackLayer.nameHash == a3_after ) || (currentBaseStateAttackLayer.nameHash == none ) || (currentBaseStateAttackLayer.nameHash == ShieldBash )) )
			{
				anim.SetBool("Defence", true);
			}
			if(Input.GetButton("Fire2") == false)
			{
				anim.SetBool("Defence", false);
			}
			if(Input.GetButton("Fire1") && currentBaseStateAttackLayer.nameHash == DefenceState)
			{
				StartCoroutine("ShieldBash_attack");
				GameObject.Find ("SoundEffectPlayer").SendMessage("FSound"); // emit fighting sound fx
			}

		}

	}
	void WalkingForward(float inputAxisSpeed)
	{
		if ( (currentBaseStateBattleMoveLayer.nameHash == walkingBattle || currentBaseStateBattleMoveLayer.nameHash == walking) && (currentBaseStateAttackLayer.nameHash == none ))  {
			gameObject.rigidbody.velocity = transform.forward * maxSpeed * inputAxisSpeed ;		

		}
	}

	void WalkingSound()
	{
		if( ((currentBaseStateBattleMoveLayer.nameHash == walkingBattle) || (currentBaseLayer.nameHash == walking) ) && (currentBaseStateAttackLayer.nameHash == none) )
		{	
			if (audio.clip == null){
				audio.clip = walkingSound;
				audio.Play ();
				audio.loop = true;
			}
		} else {
			audio.clip = null;
			audio.Play ();

		}
	}
	void ResetMecanimStates()
	{

	}

	void HorizontalVeritcalZero()
	{
		h = 0.0f;
		v = 0.0f;
		anim.SetFloat ("Speed", 0.0f);				
		anim.SetFloat ("Direction", 0.0f); 	
	}
}
