﻿using UnityEngine;
using System.Collections;

public class Wojownik : MonoBehaviour {

	public Animator anim;
	//public bool readyForBattle;
	//public bool battle;
	
	//public float DirectionDampTime = .25f;
	
	void Start () 
	{
		anim = gameObject.GetComponent<Animator>();
	}
	
	void Update () 
	{		
		// PODNOSI BRON
		DrawWeapon ();
			

			
			//battle = true;
			//get the current state
			AnimatorStateInfo stateInfo = anim.GetCurrentAnimatorStateInfo(1); // cyfra oznacza numer warstwy w kolejnosci od 0,1,2,3...
			
			//if we're in "Run" mode, respond to input for jump, and set the Jump parameter accordingly. 
			/*
			 	if (stateInfo.nameHash == Animator.StringToHash ("Przejscie.DoBitwy")) {
						anim.SetBool ("ReadyForBattle", true);
						//readyForBattle = true;
				}
			*/
		// BattleActions
		BattleActions ();

		// Współprogramy z licznikiem czasowym z opóźnieniem dla ataków i obrony


			/*
			float h = Input.GetAxis("Horizontal");
			float v = Input.GetAxis("Vertical");
			
			//set event parameters based on user input
			animator.SetFloat("Speed", h*h+v*v);
			animator.SetFloat("Direction", h, DirectionDampTime, Time.deltaTime);
			*/
		       
	}     
	// IENUMERATORY POWINNY BYC POZA FUNKCJA UPDATE
	IEnumerator AtakH() {
		anim.SetBool("AtakH", true );
		yield return new WaitForSeconds(0.1f);
		anim.SetBool("AtakH", false);
		
		
	}
	
	IEnumerator AtakV() {
		anim.SetBool("AtakV", true);
		yield return new WaitForSeconds(0.1f);
		anim.SetBool("AtakV", false);
		
		
	}
	/*IEnumerator Obrona(){
			anim.SetBool("Defence", true);
			yield return new WaitForSeconds(0.1f);
			anim.SetBool("Defence", true);
		
			
		}*/
	IEnumerator PodniesBron() {

				yield return new WaitForSeconds (0.6f);
				anim.SetBool ("ReadyForBattle", true);	
		}
	// PODNOSI BRON
	void DrawWeapon(){

		if (Input.GetButtonDown("GetWeapon")) 
		{
			if ( anim.GetBool("Battle")==false && anim.GetBool("ReadyForBattle")==false )
			{
				anim.SetBool("Battle", true);
				StartCoroutine("PodniesBron");
			}
			else
			{
				anim.SetBool ("ReadyForBattle", false);	
				anim.SetBool ("Battle", false);	
				
			}
		}	

	}

	//BattleActions
	void BattleActions(){
		if ( anim.GetBool("Battle")==true && anim.GetBool("ReadyForBattle")==true)
		{
			//if (battle==true && readyForBattle==true){
			if (Input.GetButtonDown("Atak1")) {
				//anim.SetBool("AtakH", true);
				StartCoroutine("AtakH");
			}		
			if (Input.GetButtonDown("Atak2")) {
				StartCoroutine("AtakV");
				//anim.SetBool("AtakV", true);
			}
			if (Input.GetButton("Obrona")) {
				
				anim.SetBool("Defence", true);
			}
			if (Input.GetButtonUp("Obrona")) {
				
				anim.SetBool("Defence", false);
			}
			
			
		}
	}

}
