﻿using UnityEngine;
using System.Collections;

public class Walk : MonoBehaviour {
	
		
		public float maxSpeed = 5f;
		public float maxGoraDolSpeed = 5f;
		public float maxRotation = 2f;
		public static int AI = 0;
		
		
		Animator anim;
					
		// Use this for initialization
		void Start (){
			anim = GetComponent<Animator>();			
		}
		
		// Update is called once per frame
		void FixedUpdate () {
			
			//bieg do przodu do tyłu
			float Move = Input.GetAxis("Vertical"); // Oś input
			anim.SetFloat ("Speed", Move  ); // string to parametr w animatorze
			//	GameObject.Find("Character").rigidbody2D.velocity = new Vector2( PrawoLewoMove * maxSpeed, rigidbody2D.velocity.y );
			/*
			//bieg w gore i dol
			float GoraDolMove = Input.GetAxisRaw ("Vertical"); // Oś input
			anim.SetFloat ("GoraDolSpeed", GoraDolMove); // string to parametr w animatorze
			//	GameObject.Find("Character").rigidbody2D.velocity = new Vector2( rigidbody2D.velocity.x, GoraDolMove * maxGoraDolSpeed);
			*/
			/*
		// wersja na 3d
		gameObject.rigidbody.velocity = new Vector3(rigidbody.velocity.x , rigidbody.velocity.y  , Move * maxSpeed );
		*/	
			// wersja na 2d
			// pobiera input do okreslenia obrotu postaci w osi z
			float Rotation = Input.GetAxis ("Horizontal");
			//	gameObject.rigidbody.velocity = new Vector2 (Move * maxSpeed, rigidbody.velocity.y);
			gameObject.rigidbody.velocity = transform.forward * maxSpeed * Move;
		    gameObject.rigidbody.angularVelocity = new Vector3 (rigidbody.angularVelocity.x, Rotation * maxRotation ,rigidbody.angularVelocity.z ); 
			// rotacja wymaga poprawek, postac nie idze w kierunku patrzenia tylko caly czas w x
		}
		
		void Update() 
		{ 
			if(Input.GetButtonDown("Fire1")){
				
				StartCoroutine("czekaj");
				
			}
			
		}
		
		
	}
	
