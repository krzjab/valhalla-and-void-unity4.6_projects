﻿using UnityEngine;
using System.Collections;


public class BarieraBarSc : MonoBehaviour {

	public float emotionBar;
	public float emotionBarMax;
	public int emotionBarSW;

	public bool chainCreationEnabled;
	private Transform spot1;
	private Transform spot2;
	private Transform spot3;
	private Transform spot4;
	public GameObject chainPrefab;


	// Use this for initialization
	void Start () {
		emotionBarSW = 11;
		emotionBar = 11f;
		emotionBarMax = 11f;
		chainCreationEnabled = true;
	}
	
	// Update is called once per frame
	void Update () {

		if((chainCreationEnabled== true) && emotionBar<=0)
		{	
			//StartCoroutine("CreateChains");
			GameObject.Find ("CameraAnimationHolder").SendMessage("StartCameraAnimation");
			chainCreationEnabled = false;
		}


	}

	IEnumerator CreateChains()
	{	
		yield return new WaitForSeconds (0.001f);
		spot1 = GameObject.Find ("ChainSpot1").transform;
		GameObject CP1 = Instantiate(chainPrefab, spot1.position , Quaternion.identity) as GameObject;
		CP1.name = "Chain";
		spot2 = GameObject.Find ("ChainSpot2").transform;
		GameObject CP2 = Instantiate(chainPrefab, spot2.position , Quaternion.identity) as GameObject;
		CP2.name = "Chain";
		spot3 = GameObject.Find ("ChainSpot3").transform;
		GameObject CP3 = Instantiate(chainPrefab, spot3.position , Quaternion.identity) as GameObject;
		CP3.name = "Chain";
		spot4 = GameObject.Find ("ChainSpot4").transform;
		GameObject CP4 = Instantiate(chainPrefab, spot4.position , Quaternion.identity) as GameObject;
		CP4.name = "Chain";
	}

	void ChangeInfoBackGround()
	{	
		switch(emotionBarSW)
		{
		case 11:
			GameObject.Find("InfoBackgroundGeneral").GetComponent<guiTextBox>().textView = "null";
			break;
		case 10:
			GameObject.Find("InfoBackgroundGeneral").GetComponent<guiTextBox>().textView = "Meind is playing tricks on you.";
			GameObject.Find("InfoBackgroundVoice").GetComponent<guiTextBox>().textView = "Voice: Be vigilant!";
			break;
		case 9:
			GameObject.Find("InfoBackgroundGeneral").GetComponent<guiTextBox>().textView = "Seek for yourself.";
			GameObject.Find("InfoBackgroundVoice").GetComponent<guiTextBox>().textView = "Voice: Get some insight!";
			break;
		case 8:
			GameObject.Find("InfoBackgroundGeneral").GetComponent<guiTextBox>().textView = "Gloomy fog of weirdness. ";
			break;
		case 7:
			GameObject.Find("InfoBackgroundGeneral").GetComponent<guiTextBox>().textView = "Slimy dark mud.";
			break;
		case 6:
			GameObject.Find("InfoBackgroundGeneral").GetComponent<guiTextBox>().textView = "Slimy dark mud.";
			GameObject.Find("InfoBackgroundVoice").GetComponent<guiTextBox>().textView = "Voice: Strive, and do not yield!";
			break;
		case 5:
			GameObject.Find("InfoBackgroundGeneral").GetComponent<guiTextBox>().textView = "This is Madness!";
			break;
		case 4:
			GameObject.Find("InfoBackgroundGeneral").GetComponent<guiTextBox>().textView = "It is everywhere.";
			break;
		case 3:
			GameObject.Find("InfoBackgroundGeneral").GetComponent<guiTextBox>().textView = "It is everywhere.";
			GameObject.Find("InfoBackgroundVoice").GetComponent<guiTextBox>().textView = "Voice: Embrace yourself. Move!";
			break;
		case 2:
			GameObject.Find("InfoBackgroundGeneral").GetComponent<guiTextBox>().textView = "It is everywhere.";
			break;
		case 1:
			GameObject.Find("InfoBackgroundGeneral").GetComponent<guiTextBox>().textView = "The last one!.";
			break;
		case 0:
			GameObject.Find("InfoBackgroundGeneral").GetComponent<guiTextBox>().textView = "What now?";
			GameObject.Find("InfoBackgroundVoice").GetComponent<guiTextBox>().textView = "Voice: Destroy the chains,\nleave them to me!";
			GameObject.Find("InfoFront").GetComponent<guiTextBox>().textView = "Voice: Break the chains,\nleave them to me!";
			break;
		}
	}
	// public static Object Instantiate(Object original, Vector3 position, Quaternion rotation);
}
