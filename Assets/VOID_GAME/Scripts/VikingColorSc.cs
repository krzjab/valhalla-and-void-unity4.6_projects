﻿using UnityEngine;
using System.Collections;

public class VikingColorSc : MonoBehaviour {


	public Texture skin;

	public Texture armor;
	public Texture armorBump;

	public string opis;
	public Renderer[] Renderers;	
	public Material[] currentMaterials;
	public Color kolorBrown;
	public Color kolorMetal;

	// Use this for initialization
	void Start () {
		kolorBrown.r = 0.192f;
		kolorBrown.g = 0.16f;
		kolorBrown.b = 0.16f;
		kolorBrown.a = 1f;
		kolorMetal.r = 0.5f;
		kolorMetal.g = 0.5f;
		kolorMetal.b = 0.5f;
		kolorMetal.a = 1f;
		//currentMaterials = gameObject.GetComponentInChildren<Renderer> ().materials;
		VikingColorsOFF ();

	}
	
	// Update is called once per frame
	void Update () {


	}
	void VikingColorsOFF()
	{
		// gets all renderers -> gets ale materrials in each renderer -> changes texture and color
		Renderers = gameObject.GetComponentsInChildren<Renderer> ();
		foreach (Renderer ren in Renderers) {
			currentMaterials = ren.materials;
			foreach (Material mat in currentMaterials) {
				// opis materiału
				Debug.Log (mat.name);
				// this statement work  :D:D:D:D:D: :D
				if (mat.name == "skora_ciemna (Instance)") {
					mat.color = Color.white;
					mat.mainTexture = null;
					Debug.Log ("dziala warunek");
				} else if (mat.name == "brushed-metal-texture (Instance)") {
					mat.color = Color.white;
					mat.mainTexture = null;
				} else if (mat.name == "cialo_color_v05 (Instance)") {
					mat.color = Color.white;
					mat.mainTexture = null;
				} else if (mat.name == "napiersnik02 (Instance)") {
					mat.color = Color.white;
					mat.mainTexture = null;
					
				}
			}
		}
	}
	void VikingColorsON()
	{
		// gets all renderers -> gets ale materrials in each renderer -> changes texture and color
		Renderers = gameObject.GetComponentsInChildren<Renderer> ();
		foreach (Renderer ren in Renderers) {
			currentMaterials = ren.materials;
			foreach (Material mat in currentMaterials) {
				// opis materiału
				Debug.Log (mat.name);
				// this statement work  :D:D:D:D:D: :D
				if (mat.name == "skora_ciemna (Instance)") {
					mat.color = kolorBrown;
					mat.mainTexture = null;
					Debug.Log ("dziala warunek");
				} else if (mat.name == "brushed-metal-texture (Instance)") {
					mat.color = kolorMetal;
					mat.mainTexture = null;
				} else if (mat.name == "cialo_color_v05 (Instance)") {
					mat.color = kolorMetal;
					mat.mainTexture = skin;
				} else if (mat.name == "napiersnik02 (Instance)") {
					mat.color = kolorMetal;
					mat.mainTexture = armor;
					
				}
			}
		}
	}

}
