﻿using UnityEngine;
using System.Collections;

public class NB3 : MonoBehaviour {
	
	/*
	 * Goal of this script is to control the "Next3" button functions and GUI.
	 */ 
	
	public string levelToLoad; // which level to load 
	
	public AudioClip buttonClip; // sound file of clicked button
	
	// Textures of the GUI button in normal, and rollOver state
	public Texture2D rollOverTexture;
	public Texture2D normalTexture;
	public Texture2D mouseUpTexture;
	
	//Buttons activ / disabled
	public bool buttonActive = true;
	
	// Initialization
	void Start () {
		
	}
	
	void OnMouseEnter(){
		// changes button texture when mouse cursor is on the button
		if ( buttonActive ) 
		{
			guiTexture.texture = rollOverTexture;
		}
	}
	
	void OnMouseExit(){
		// changes the texture when mouse cursor exits the button
		if (buttonActive)
		{
			guiTexture.texture = normalTexture;
		}
	}
	
	IEnumerator OnMouseUp(){
		// Loading next level and giving a sound signal when LMB is clicked over the button.
		// GUI changes and sound signal when LMB is clicked over the button.
		if (buttonActive) {
			audio.PlayOneShot (buttonClip);
			guiTexture.texture = mouseUpTexture;
			buttonActive = false;


			yield return new WaitForSeconds (1f);
			Application.LoadLevel (levelToLoad);

		}
	}
	
	
	
	
}
