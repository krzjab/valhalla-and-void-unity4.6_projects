﻿using UnityEngine;
using System.Collections;

public class JumpGuiSc : MonoBehaviour {

	public GUITexture[] textures;

	// Use this for initialization
	IEnumerator Start () {
		yield return new WaitForSeconds (0.001f);
		textures = gameObject.GetComponentsInChildren<GUITexture> ();
		DisableOnStart ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void DisableOnStart()
	{
		foreach (GUITexture gt in textures) {
			gt.enabled = false;
		}
		gameObject.GetComponent<guiTextBox> ().guiTextLabelON = false;
	}
	void EnableJumpGui()
	{
		foreach (GUITexture gt in textures) {
			gt.enabled = true;
		}
		gameObject.GetComponent<guiTextBox> ().guiTextLabelON = true;
	}
}
