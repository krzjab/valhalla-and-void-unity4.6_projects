﻿using UnityEngine;
using System.Collections;

public class backgroundWhiteAnim : MonoBehaviour {

	private Animator anim;
	private Color wygaszony;
	public AudioClip soundFx;
	
	// Use this for initialization
	void Start () {
		wygaszony.a = 0f;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void EndAnimation()
	{
		gameObject.GetComponent<Animator> ().enabled = false;
		gameObject.GetComponent<GUITexture> ().color = wygaszony;
		gameObject.GetComponent<GUITexture> ().enabled = false;
	}
	void StartAnimation()
	{
		gameObject.GetComponent<Animator> ().enabled = true;
		gameObject.GetComponent<GUITexture> ().enabled = true;
	}
	void SoundFx()
	{
		audio.PlayOneShot (soundFx);
	}
}
