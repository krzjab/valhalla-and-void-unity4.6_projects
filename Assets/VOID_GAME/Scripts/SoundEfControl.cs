﻿using UnityEngine;
using System.Collections;
[RequireComponent (typeof(AudioSource))]

public class SoundEfControl : MonoBehaviour {
	/*
	 *  Script controls menagement of sound effects in the game.
	 * 
	 */ 
	public AudioClip sound1;
	public AudioClip sound2;
	public AudioClip paper1;
	public AudioClip paper2;
	public AudioClip barierSound;
	public AudioClip chainShowSound;
	public AudioClip chainHitSound;
	public AudioClip chainDestroySound;
	public AudioClip fireFxHit;
	public AudioClip fireFxLong;
	public AudioClip checkpointSound;

	//fighting sounds
	//short
	public AudioClip f1sound;
	public AudioClip f2sound;
	public AudioClip f3sound;
	public AudioClip f4sound;
	public AudioClip f5sound;
	//long
	public AudioClip f6sound;
	public AudioClip f7sound;
	public AudioClip f8sound;
	public AudioClip f9sound;
	public AudioClip f10sound;

	public AudioClip swordSwing1;
	public AudioClip swordSwing2;
	public AudioClip swordSwing3;

	public AudioClip pickUp1;
	public AudioClip pickUp2;
	public AudioClip pickUp3;

	public AudioClip SoulVertexDestroy1;
	public AudioClip SoulVertexDestroy2;
	public AudioClip SoulVertexDestroy3;

	// Use this for initialization
	void Start () {
		audio.playOnAwake = false;
		audio.loop = false;
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	

	void Sound1()
	{
		gameObject.GetComponent<AudioSource> ().clip = sound1;
		audio.Play ();
	}
	void Sound2()
	{
		gameObject.GetComponent<AudioSource> ().clip = sound2;
		audio.Play ();
	}
	void PaperSound1()
	{
		gameObject.GetComponent<AudioSource> ().clip = paper1;
		audio.Play ();
	}
	void PaperSound2()
	{
		gameObject.GetComponent<AudioSource> ().clip = paper2;
		audio.Play ();
	}
	void BarierSound()
	{
		gameObject.GetComponent<AudioSource> ().clip = barierSound;
		audio.Play ();
	}
	void ChainShowSound()
	{
		gameObject.GetComponent<AudioSource> ().clip = chainShowSound;
		audio.Play ();
	}
	void ChainHitSound()
	{
		gameObject.GetComponent<AudioSource> ().clip = chainHitSound;
		audio.Play ();
	}
	void ChainDestroySound()
	{
		gameObject.GetComponent<AudioSource> ().clip = chainDestroySound;
		audio.Play ();
	}

	void ChainDestroySound2()
	{
		switch(Random.Range(1,4))
		{
		case 1:
			audio.PlayOneShot (SoulVertexDestroy1);
			break;
		case 2:
			audio.PlayOneShot (SoulVertexDestroy2);
			break;
		case 3:
			audio.PlayOneShot (SoulVertexDestroy3);
			break;
			
		}
		audio.Play ();
	}


	void FireFxSound()
	{
		gameObject.GetComponent<AudioSource> ().clip = fireFxLong;
		audio.Play ();
		audio.PlayOneShot (fireFxHit);
	}
	void CheckpointSound()
	{
		gameObject.GetComponent<AudioSource> ().clip = checkpointSound;
		audio.Play ();
	}
	void FSound()
	{
		switch(Random.Range(1,11))
		{
			case 1:
				gameObject.GetComponent<AudioSource> ().clip = f1sound;
				break;
			case 2:
				gameObject.GetComponent<AudioSource> ().clip = f2sound;
				break;
			case 3:
				gameObject.GetComponent<AudioSource> ().clip = f3sound;
				break;
			case 4:
				gameObject.GetComponent<AudioSource> ().clip = f4sound;
				break;
			case 5:
				gameObject.GetComponent<AudioSource> ().clip = f5sound;
				break;
			case 6:
				gameObject.GetComponent<AudioSource> ().clip = f6sound;
				break;
			case 7:
				gameObject.GetComponent<AudioSource> ().clip = f7sound;
				break;
			case 8:
				gameObject.GetComponent<AudioSource> ().clip = f8sound;
				break;
			case 9:
				gameObject.GetComponent<AudioSource> ().clip = f9sound;
				break;
			case 10:
				gameObject.GetComponent<AudioSource> ().clip = f10sound;
				break;
		}
		audio.Play ();

	}
	void SwordSwing1()
	{
		audio.PlayOneShot (swordSwing1);
	}
	void SwordSwing2()
	{
		audio.PlayOneShot (swordSwing2);
	}
	void SwordSwing3()
	{
		audio.PlayOneShot (swordSwing3);
	}

	void PickUp1()
	{
		audio.PlayOneShot (pickUp1);
	}
	void PickUp2()
	{
		audio.PlayOneShot (pickUp2);
	}
	void PickUp3()
	{
		audio.PlayOneShot (pickUp3);
	}
	void PickUp()
	{
		switch(Random.Range(1,4))
		{
		case 1:
			gameObject.GetComponent<AudioSource> ().clip = pickUp1;
			break;
		case 2:
			gameObject.GetComponent<AudioSource> ().clip = pickUp2;
			break;
		case 3:
			gameObject.GetComponent<AudioSource> ().clip = pickUp3;
			break;
		
		}
		audio.Play ();
	}
}
