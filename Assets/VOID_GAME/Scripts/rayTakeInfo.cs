﻿using UnityEngine;
using System.Collections;

public class rayTakeInfo : MonoBehaviour {

	RaycastHit hit;
	GameObject hitObject ;
	GameObject HealthbarObj;


	// Use this for initialization
	void Start () {
		HealthbarObj = GameObject.Find ("Healthbar");
	}
	
	// Update is called once per frame
	void Update () {
		//Raycasting nie działa na triger, raycasting działa na colider

		if (Physics.Raycast(transform.position, transform.forward, out hit, 1)) 
		{
			if(hit.collider.gameObject.tag=="Chain" && HealthbarObj.GetComponent<GUITexture>().enabled == false)
			{	
				//reamember colliders must not cross eachother -->> bugs

				//get objectName
				HealthbarObj.GetComponent<guiTextBox>().textView = "Souls Vertex";

				hit.collider.gameObject.SendMessage("ChainInfo");
				// get Chain hp and Update Healthbar

				hit.collider.gameObject.GetComponent<chainControl>().GuiUpdate();

				// end
				HealthbarObj.GetComponent<GUITexture>().enabled = true;
				HealthbarObj.GetComponent<guiTextBox>().enabled = true;
				GameObject.Find("HealthbarBack").GetComponent<GUITexture>().enabled = true;
				Debug.Log ("ON raycasting");
				//Debug.Log("POBIERASZ informacje z chaina");

			}else if(hit.collider.gameObject.tag=="Enemy" && HealthbarObj.GetComponent<GUITexture>().enabled == false){
				HealthbarObj.GetComponent<guiTextBox>().textView = "Zombie Guardian";
				hit.collider.gameObject.SendMessage("ChainInfo");
				// get Chain hp and Update Healthbar
				
				hit.collider.gameObject.GetComponent<chainControl>().GuiUpdate();
				
				// end
				HealthbarObj.GetComponent<GUITexture>().enabled = true;
				HealthbarObj.GetComponent<guiTextBox>().enabled = true;
				GameObject.Find("HealthbarBack").GetComponent<GUITexture>().enabled = true;
				Debug.Log ("ON raycasting");
				//Debug.Log("POBIERASZ informacje z chaina");
			}
		} else{
			if(GameObject.Find("Healthbar").GetComponent<GUITexture>().enabled == true){
				GameObject.Find("Healthbar").GetComponent<GUITexture>().enabled = false;
				GameObject.Find("Healthbar").GetComponent<guiTextBox>().enabled = false;
				GameObject.Find("HealthbarBack").GetComponent<GUITexture>().enabled = false;
				Debug.Log ("OFF Raycastingu");
			}
		}
	}



}
