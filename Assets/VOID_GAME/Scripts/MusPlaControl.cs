﻿using UnityEngine;
using System.Collections;
[RequireComponent (typeof(AudioSource))]

public class MusPlaControl : MonoBehaviour {
	/*
	 *  Script controls menagement of music clips in the game.
	 * 
	 */
	public AudioClip music1Gloom;
	public AudioClip music2War;
	public AudioClip music3Light;
	public AudioClip music4WontSurrender;

	// Use this for initialization
	void Start () {
		audio.playOnAwake = true;
		audio.loop = true;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void EndMusic()
	{
		gameObject.GetComponent<AudioSource> ().clip = null;
	}
	void StartMusic1()
	{
		gameObject.GetComponent<AudioSource> ().clip = music1Gloom;
		audio.Play ();
	}
	void StartMusic2()
	{
		gameObject.GetComponent<AudioSource> ().clip = music2War;
		audio.Play ();
	}
	void StartMusic3()
	{
		gameObject.GetComponent<AudioSource> ().clip = music3Light;
		audio.Play ();
	}
	void StartMusic4()
	{
		gameObject.GetComponent<AudioSource> ().clip = music4WontSurrender;
		audio.Play ();
	}

}
