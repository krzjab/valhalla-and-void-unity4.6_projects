﻿using UnityEngine;
using System.Collections;

public class BarierParentSc : MonoBehaviour {

	public float bbHP;
	public float bbHPmax;
	public int bbHPSW;
	public bool DeactivateBP;
	private ParticleSystem[] bPS;

	private Vector3 teleportSpotData;

	// Use this for initialization
	void Start () {
		bbHPSW = 4;
		bbHP = 4f;
		bbHPmax = 4f;
		DeactivateBP = true;
	}
	
	// Update is called once per frame
	void Update () {
		if ((bbHP == 0) && DeactivateBP == true) 
		{
			DeactivateBP = false;
			Debug.Log ("Dezaktywacja barier");
			/*  tutaj miejsce na uruchomienie efektu specjalnego przy zniszczeniu wszystkiego
			*	byc moze wstaw tu efekt czasteczkowy i oglad kamery z oddali
			*/

			// deactivate Barier parent
			StartCoroutine("DestruktOnTime",2.5f);
		}
	
	}

	void DemageBB(int demage)
	{
		this.bbHP = this.bbHP - demage;
		this.bbHPSW = this.bbHPSW - demage;
	}
	IEnumerator DestruktOnTime(float wait)
	{	
		// wait for the last chain desintegration
		yield return new WaitForSeconds (2.2f);
		//set particle effect of destruction
		GameObject.Find ("FlameLightFollow").SendMessage ("FlamesOn");
		// teleport Player to the center, and orto camera
		GameObject.Find ("VIKING").transform.position = GameObject.Find ("TeleportSpot").transform.position;
		teleportSpotData = GameObject.Find ("TeleportSpot").GetComponent<Transform> ().position;
		GameObject.Find ("Main Camera ORTO").GetComponent<Transform> ().position = new Vector3 (teleportSpotData.x, 4, teleportSpotData.z);
		// disable movement of player
		//GameObject.Find ("VIKING").SendMessage ("DisableBattleStates");
		GameObject.Find ("VIKING").SendMessage ("DisableControls");
		GameObject.Find ("VIKING").SendMessage ("HorizontalVeritcalZero");
		// set camera animation movement
		GameObject.Find ("CameraAnimationHolder2").SendMessage ("StartCameraAnimation2");
		// wait
		yield return new WaitForSeconds(wait);
		//set Viking colors ONN
		GameObject.Find ("VIKING").SendMessage ("VikingColorsON");
		//wait
		yield return new WaitForSeconds (1f);
		// destruct
		bPS = gameObject.GetComponentsInChildren<ParticleSystem> ();
		foreach (ParticleSystem ps in bPS) {

			ps.loop = false;
		}
		yield return new WaitForSeconds (2.5f);
		gameObject.SetActive (false);
	}

	IEnumerator ChangeInfoBackGround()
	{	
		switch(bbHPSW)
		{
		
		case 4:
			GameObject.Find("InfoBackgroundGeneral").GetComponent<guiTextBox>().textView = "null";
			break;
		case 3:
			GameObject.Find("InfoBackgroundGeneral").GetComponent<guiTextBox>().textView = "Yeah, it works";
			GameObject.Find("InfoBackgroundVoice").GetComponent<guiTextBox>().textView = "Voice: Do it again.";
			break;
		case 2:
			GameObject.Find("InfoBackgroundGeneral").GetComponent<guiTextBox>().textView = "Easyer than I thought";
			break;
		case 1:
			GameObject.Find("InfoBackgroundGeneral").GetComponent<guiTextBox>().textView = "No more of this cage"; 
			GameObject.Find("InfoBackgroundVoice").GetComponent<guiTextBox>().textView = "Voice: Be vigilant! Strive!";
			break;
		case 0:
			yield return new WaitForSeconds(3f);
			GameObject.Find("InfoBackgroundGeneral").GetComponent<guiTextBox>().textView = "Extraordinary.";
			GameObject.Find("InfoBackgroundVoice").GetComponent<guiTextBox>().textView = "Voice: Every time when a man\nhas a chance to specify his\nidentity...";
			GameObject.Find("InfoFront").GetComponent<guiTextBox>().textView = "Voice: Follow the light\nthe path is narrow.";
			break;
		}
	}

}
