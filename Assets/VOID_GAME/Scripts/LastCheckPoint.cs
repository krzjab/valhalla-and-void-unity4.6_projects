﻿using UnityEngine;
using System.Collections;

public class LastCheckPoint : MonoBehaviour {

	public bool checkPointActive;
	
	// Use this for initialization
	void Start () {
		checkPointActive = true;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void OnTriggerEnter(Collider col) {
		if (col.gameObject.tag == "Player" && checkPointActive == true) 
		{
			// metoda
			EnableJumpGui();
			checkPointActive = false;
		}
	}
	
	void EnableJumpGui(){
		GameObject.Find ("JumpGui").SendMessage ("EnableJumpGui");
	}
}
