﻿using UnityEngine;
using System.Collections;

public class textAnimationSc : MonoBehaviour {

	public bool animationEnabler;
	public float EndY;
	public float speedRegulator;
	// Use this for initialization
	void Start () {
		this.speedRegulator = 0.03f;
		this.EndY = 0.5f;
		gameObject.GetComponent<Transform> ().position = new Vector3 (gameObject.transform.position.x, -0.33f, transform.position.z);
		animationEnabler = false;
	}
	
	// Update is called once per frame
	void Update () {
		StartCoroutine("TextAnimation");
	}

	void TextAnimation()
	{
		if (animationEnabler == true && this.gameObject.transform.position.y < this.EndY)
		{
			gameObject.transform.position = gameObject.transform.position + new Vector3(0, Time.deltaTime*speedRegulator,0);
		} else if ( this.gameObject.transform.position.y >= this.EndY )
		{
			animationEnabler = false;
		}
	}

	void EnableTextAnimation()
	{
		animationEnabler = true;
	}

}
