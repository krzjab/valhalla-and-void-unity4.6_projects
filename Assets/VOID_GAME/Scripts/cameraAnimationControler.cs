﻿using UnityEngine;
using System.Collections;

public class cameraAnimationControler : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	IEnumerator StartCameraAnimation()
	{	
		//animation and camera
		gameObject.GetComponentInChildren<Camera> ().enabled = true;
		gameObject.GetComponentInChildren<Animator> ().enabled = true;
		//gui control
		GameObject.Find ("InfoBackgroundVoice").SendMessage ("GuiViewOFF");
		GameObject.Find ("InfoBackgroundGeneral").SendMessage ("GuiViewOFF");
		GameObject.Find ("InfoFront").SendMessage ("GuiViewOFF");
		GameObject.Find ("BarierBar").SendMessage ("GuiViewOFF");
		GameObject.Find ("BarierBarBack").SendMessage ("GuiViewOFF");
		GameObject.Find ("Help").GetComponent<GUITexture> ().enabled = false;

		GameObject.Find ("MusicPlayer").SendMessage("StartMusic2");
		//wait and use chain Initialization method
		yield return new WaitForSeconds (1f);
		GameObject.Find ("BarierBar").SendMessage ("CreateChains");
		// play sound of chain Show
		GameObject.Find ("SoundEffectPlayer").SendMessage("ChainShowSound");
		//Controlls
		GameObject.Find ("VIKING").SendMessage ("DisableControls");
		GameObject.Find ("VIKING").SendMessage ("HorizontalVeritcalZero");

	}
	void EndCameraAnimation()
	{	//aniamtion and camera
		gameObject.GetComponentInChildren<Camera> ().enabled = false;
		gameObject.GetComponentInChildren<Animator> ().enabled = false;
		// enable gui's
		GameObject.Find ("InfoBackgroundVoice").SendMessage ("GuiViewON");
		GameObject.Find ("InfoBackgroundGeneral").SendMessage ("GuiViewON");
		GameObject.Find ("InfoFront").SendMessage ("GuiViewON");
		// barier bar disabled
		GameObject.Find ("BarierBar").SendMessage ("GuiViewON");
		GameObject.Find ("BarierBarBack").SendMessage ("GuiViewON");
		GameObject.Find ("Help").GetComponent<GUITexture> ().enabled = true;
		//Controlls
		GameObject.Find ("VIKING").SendMessage ("EnableControls");


	}
	void StartCameraAnimation2()
	{
		//animation and camera
		gameObject.GetComponentInChildren<Camera> ().enabled = true;
		gameObject.GetComponentInChildren<Animator> ().enabled = true;
		//gui control
		GameObject.Find ("InfoBackgroundVoice").SendMessage ("GuiViewOFF");
		GameObject.Find ("InfoBackgroundGeneral").SendMessage ("GuiViewOFF");
		GameObject.Find ("InfoFront").SendMessage ("GuiViewOFF");
		GameObject.Find ("BarierBar").SendMessage ("GuiViewOFF");
		GameObject.Find ("BarierBarBack").SendMessage ("GuiViewOFF");
		GameObject.Find ("Help").GetComponent<GUITexture> ().enabled = false;
		//fire sound fx
		GameObject.Find ("SoundEffectPlayer").SendMessage("FireFxSound"); //fire fx sounds
		// music change
		GameObject.Find ("MusicPlayer").SendMessage("StartMusic3");
	}
	IEnumerator EndCameraAnimation2()
	{
		//aniamtion and camera
		gameObject.GetComponentInChildren<Camera> ().enabled = false;
		gameObject.GetComponentInChildren<Animator> ().enabled = false;
		// enable gui's
		GameObject.Find ("InfoBackgroundVoice").SendMessage ("GuiViewON");
		GameObject.Find ("InfoBackgroundGeneral").SendMessage ("GuiViewON");
		GameObject.Find ("InfoFront").SendMessage ("GuiViewON");
		// disable gloom bar
		//GameObject.Find ("BarierBar").SendMessage ("GuiViewON");
		//GameObject.Find ("BarierBarBack").SendMessage ("GuiViewON");
		GameObject.Find ("Help").GetComponent<GUITexture> ().enabled = true;
		// disable particle emition, change lighting, enable movement to Player
		GameObject.Find ("FlameLightFollow").SendMessage ("FlamesOff");
		GameObject.Find ("VIKING").SendMessage ("EnableControls");
		// flame gui ON
		GameObject.Find ("LifeBar").SendMessage ("GuiViewON");
		GameObject.Find ("LifeBarBack").SendMessage ("GuiViewON");
		// enable areaLight animation
		yield return new WaitForSeconds (8f);
		GameObject.Find ("AreaLight").GetComponent<Animator> ().enabled = true;

	}
}
