﻿using UnityEngine;
using System.Collections;

public class CameraTPPVG : MonoBehaviour {

	/*
	 * 3 modes of TPP camera for manipulation of X rotation and Y position by buttons
	 * 
	 * 1: rotation X 35, position Y 2,84
	 * 2: rotation X 30, position Y 2,54
	 * 3: rotation X 25, position Y 2,04
	 * 4: rotation X 15, position Y 1,54
	 */

	private int cameraTPPMode;

	// Use this for initialization
	void Start () {
		cameraTPPMode = 1;
	
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyUp(KeyCode.R)) 
		{
			if (cameraTPPMode == 1 || cameraTPPMode == 2 || cameraTPPMode == 3)
			{
				cameraTPPMode++;
			}else{
			//	cameraTPPMode = cameraTPPMode-2;
			}
		}
		if (Input.GetKeyUp(KeyCode.F)) 
		{
			if (cameraTPPMode == 4 || cameraTPPMode == 3 || cameraTPPMode == 2)
			{
				cameraTPPMode--;
			}else{
			//	cameraTPPMode = cameraTPPMode+2;
			}
		}

		switch (cameraTPPMode) 
		{
			case 4: 
				gameObject.GetComponent<Transform>().position = new Vector3(transform.position.x,1.6f,transform.position.z);
				transform.eulerAngles = new Vector3(15f,transform.eulerAngles.y,transform.eulerAngles.z);
				gameObject.camera.fieldOfView= 25f;
				break;
			case 3: 
				gameObject.GetComponent<Transform>().position = new Vector3(transform.position.x,1.8f,transform.position.z);
				transform.eulerAngles = new Vector3(23f,transform.eulerAngles.y,transform.eulerAngles.z);
				gameObject.camera.fieldOfView= 30f;
				break;
			case 2:
				transform.position = new Vector3(transform.position.x,2.0f,transform.position.z);
				transform.eulerAngles = new Vector3(30f,transform.eulerAngles.y,transform.eulerAngles.z);
				gameObject.camera.fieldOfView= 30f;
					break;
			case 1:
				transform.position = new Vector3(transform.position.x,2.5f,transform.position.z);
				transform.eulerAngles = new Vector3(35f,transform.eulerAngles.y,transform.eulerAngles.z);
				gameObject.camera.fieldOfView= 30f;
					break;
		}
	
	}



}
