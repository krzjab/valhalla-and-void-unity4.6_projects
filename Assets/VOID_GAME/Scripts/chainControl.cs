﻿using UnityEngine;
using System.Collections;

public class chainControl : MonoBehaviour {

	private float chainHP;
	private float chainHPmax;
	private Component[] chainArms; // refers to script component to broadcast a message

	// Use this for initialization
	void Start () {
		chainHP = 10f;
		chainHPmax = 10f;

	}
	
	// Update is called once per frame
	void Update () {


		
	}

	void ChainInfo()
	{
		//Debug.Log ("Pobierasz informacje z chaina");
	}
	void Demage(float demage)
	{
		this.chainHP = this.chainHP - demage;
		Debug.Log ("HP level is:" + chainHP);
		GuiUpdate();
		ParticleHitEffect ();
		GameObject.Find ("SoundEffectPlayer").SendMessage("ChainHitSound"); // emit chain hit sound fx
		if (this.chainHP <= 0) {
			ParticleDestroyEffect ();
			StartCoroutine("WaitAndDestroyChain");
			//set barierParent demage method --->>>>
			GameObject.Find ("BarieraParent").SendMessage("DemageBB",1); 
			GameObject.Find ("BarieraParent").SendMessage("ChangeInfoBackGround"); 

		}
	}
	public void GuiUpdate()
	{
		GameObject.Find("Healthbar").SendMessage("ReduceBar", this.chainHP/this.chainHPmax);
	}

	void ParticleHitEffect()
	{
		gameObject.GetComponentInChildren<ChainHitSc>().SendMessage("EmittOneShot");

	}
	void ParticleDestroyEffect()
	{
		gameObject.GetComponentInChildren<ChainDestroySc>().SendMessage ("EmittOneShot");
	}
	IEnumerator WaitAndDestroyChain()
	{



		GameObject.FindWithTag("ScoreMenager").SendMessage("AddPoints", 100.0f); //send message to add points for destroying the soul vertex // chain

		// dezaktywacja chainArmów -->>>
		chainArms = GetComponentsInChildren<chainArmsDel>();
		foreach (chainArmsDel cAD in chainArms) {
			cAD.SendMessage("DelEmmiter");
		}
		// send message to update Information viewed on InfoFront object
		ChaingeInfoNumber ();
		GameObject.Find ("SoundEffectPlayer").SendMessage("ChainDestroySound2"); // emit chain destroy sound fx
		Debug.Log ("Teraz powinna zmienić się informacja na komunikacie wyświetlanym  na InfoFront");
		// deactivate chainParent
		yield return new WaitForSeconds (2f);
		gameObject.SetActive(false);
	}

	void ChaingeInfoNumber()
	{
		//GameObject.Find ("InfoFront").GetComponent<guiTextBox> ().infoNumber++;
		//GameObject.Find ("InfoFront").GetComponent<guiTextBox> ().infoNumber = number;
		//GameObject.Find ("InfoFront").SendMessage ("ChangeInfo");

	}

	public float GetChainHP()
	{
		return chainHP;
	}
	public float GetChainHPmax()
	{
		return chainHPmax ;
	}
		
	

}
