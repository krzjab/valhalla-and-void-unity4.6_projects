﻿using UnityEngine;
using System.Collections;
[RequireComponent (typeof(AudioSource))]

public class RB2_ST : MonoBehaviour {
	/*
	 * Goal of the script: cooperation wtih "RB2_TW" script to dispaly text and play lector audio file simultaneusly.
	 * Asign to restart button in "void game".
	 */ 
	

	public AudioClip lectors; // lectors audio file 
	public AudioClip buttonClip; // sound file of clicked button

	// Textures of the GUI button in normal, and rollOver state
	public Texture2D rollOverTexture;
	public Texture2D normalTexture;
	public Texture2D mouseUpTexture;
	
	//Buttons activ / disabled
	public bool buttonActive = true;
	
	// Initialization
	void Start () {

	}
	
	void OnMouseEnter(){
		// changes button texture when mouse cursor is on the button
		if ( buttonActive ) 
		{
			guiTexture.texture = rollOverTexture;
		}
	}
	
	void OnMouseExit(){
		// changes the texture when mouse cursor exits the button
		if (buttonActive)
		{
			guiTexture.texture = normalTexture;
		}
	}
	
	IEnumerator OnMouseUp(){
		// Giving a sound signal when LMB is clicked over the button.
		audio.PlayOneShot(buttonClip);
		guiTexture.texture = mouseUpTexture;
		//buttonActive = false;
		yield return new WaitForSeconds (2f);
		// GUI changes in the scene


	}
	// Plays the lector music file
	IEnumerator Lector(float wait){
		yield return new WaitForSeconds (wait);
		audio.PlayOneShot(lectors);
	}
	public void StartTextLector()
	{
		// audio refers to AudioSource component of this obiekt,
		audio.playOnAwake = true;
		audio.loop = true;
		audio.Play();
		// activates Lector method, wait 2 sec.
		StartCoroutine (Lector(2.0f));
	}
}
