﻿using UnityEngine;
using System.Collections;

public class barierControl : MonoBehaviour {

	private Vector3 teleportSpotData;
	public Component[] bariers;

	//Get barier bar info helpers
	private float currentAmount;
	private float maxAmount;
	private int randomRotY; // describes angle of teleported character

	public AudioClip barierSound;
	// Use this for initialization
	void Start () {
		gameObject.GetComponent<ParticleSystem> ().enableEmission = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other) {
		if (other.gameObject.tag == "Player") 
		{		
				// opcja na wszystkie na raz
				//bariersOn();
				

				teleportSpotData = GameObject.Find ("TeleportSpot").GetComponent<Transform> ().position;
				other.gameObject.transform.position = teleportSpotData;
				
				GetRandomY();
				other.gameObject.transform.Rotate(0,randomRotY,0);
				
				GameObject.Find ("Main Camera ORTO").GetComponent<Transform> ().position = new Vector3 (teleportSpotData.x, 4, teleportSpotData.z);
				
				// demage barieraBar current amount
				DemageToGloom();
				Debug.Log("Demage to gloom");
				//set emition ON
				// opcja na pojedyncze
				gameObject.GetComponent<ParticleSystem> ().enableEmission = true;
				// play barier sound
				GameObject.Find ("SoundEffectPlayer").SendMessage("BarierSound");
				// get hp info from barierBar
				GetBarierBarInfo();
				Debug.Log("Take info from bariera bar");
				//send Reducebar message
				GameObject.Find ("BarierBar").SendMessage("ReduceBar", this.currentAmount/this.maxAmount);
				Debug.Log("Reduce bariera bar gui width");

		}
		if (other.gameObject.tag == "MainCamera") 
		{
			//gameObject.GetComponent<ParticleSystem> ().enableEmission = true;
			teleportSpotData = GameObject.Find ("TeleportSpot").GetComponent<Transform> ().position;
			other.gameObject.transform.position = teleportSpotData;
			//GameObject.Find ("Main Camera ORTO").GetComponent<Transform> ().position = new Vector3 (teleportSpotData.x, 4, teleportSpotData.z);
		}
	}
	void starBarier()
	{
		gameObject.GetComponent<ParticleSystem> ().enableEmission = true;
	}
	void bariersOn() {
		bariers = GameObject.Find ("BarieraParent").GetComponentsInChildren<ParticleSystem>();
		foreach (ParticleSystem ps in bariers) {
			Debug.Log ("BARIERY powinny sie wlaczyc");
			ps.enableEmission = true;
		}
	}
	void GetBarierBarInfo()
	{
		this.currentAmount = GameObject.Find ("BarierBar").GetComponent<BarieraBarSc> ().emotionBar;
		this.maxAmount = GameObject.Find ("BarierBar").GetComponent<BarieraBarSc> ().emotionBarMax;
		GameObject.Find ("BarierBar").SendMessage ("ChangeInfoBackGround");

	}
	void DemageToGloom()
	{	
		if (gameObject.particleSystem.enableEmission == false) {
			GameObject.Find ("BarierBar").GetComponent<BarieraBarSc> ().emotionBar--;
			GameObject.Find ("BarierBar").GetComponent<BarieraBarSc> ().emotionBarSW--;
		}
	}
	void GetRandomY()
	{
		randomRotY = Random.Range (1, 360);
	}
}
