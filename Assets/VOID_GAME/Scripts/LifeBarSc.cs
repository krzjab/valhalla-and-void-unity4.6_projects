﻿using UnityEngine;
using System.Collections;

public class LifeBarSc : MonoBehaviour {

	public GUITexture[] textures;

	// Use this for initialization
	void Start () {
		StartCoroutine ("OffGui");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	IEnumerator OffGui()
	{
		yield return new WaitForSeconds (0.01f);
		gameObject.GetComponent<guiTextBox>().guiTextLabelON = false;
		textures = gameObject.GetComponentsInChildren<GUITexture> ();
		foreach (GUITexture gt in textures)
		{
			gt.enabled = false;
		}

	}
	IEnumerator OnGui()
	{
		yield return new WaitForSeconds (0.01f);
		gameObject.GetComponent<guiTextBox>().guiTextLabelON = true;
		textures = gameObject.GetComponentsInChildren<GUITexture> ();
		foreach (GUITexture gt in textures)
		{
			gt.enabled = true;
		}
		
	}
}
