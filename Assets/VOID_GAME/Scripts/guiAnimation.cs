﻿using UnityEngine;
using System.Collections;

public class guiAnimation : MonoBehaviour {
	public float waitTime;

	// Use this for initialization
	IEnumerator Start () {
		yield return new WaitForSeconds (waitTime);
		gameObject.SendMessage ("StartAnimation");
	}
	
	// Update is called once per frame
	void Update () {

	}

}
