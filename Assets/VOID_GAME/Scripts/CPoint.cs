﻿using UnityEngine;
using System.Collections;

public class CPoint : MonoBehaviour {

	public string addMessageToVoice;
	public bool checkPointActive;

	// Use this for initialization
	void Start () {
		checkPointActive = true;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider col) {
		if (col.gameObject.tag == "Player" && checkPointActive == true) 
		{
			UpdateVoiceMessage();
			checkPointActive = false;
		}
	}

	void UpdateVoiceMessage(){
		GameObject.Find("InfoBackgroundVoice").GetComponent<guiTextBox>().textView = "Voice: " + this.addMessageToVoice;
		}
}
