﻿using UnityEngine;
using System.Collections;

public class swordControler : MonoBehaviour {

	private int a1State;
	private int a2State;
	private int a3State;
	private int a4ShB;
	private int MecanimState;
	private bool canAddDemage;
	private float animationAttackTime = 0.44f; // time to pause trigger collision for demage adding

	//variables to control changes of states to prevent conuting 
	//damege multiple times for the same hit


	// Use this for initialization
	void Start () {
		a1State = GameObject.Find ("VIKING").GetComponent<VIKING_Controler> ().attack1;
		a2State = GameObject.Find ("VIKING").GetComponent<VIKING_Controler> ().attack2;
		a3State = GameObject.Find ("VIKING").GetComponent<VIKING_Controler> ().attack3;
		a4ShB = GameObject.Find ("VIKING").GetComponent<VIKING_Controler> ().ShieldBash;
		canAddDemage = true;


	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider col)
	{	

			MecanimState = GameObject.Find ("VIKING").GetComponent<VIKING_Controler> ().currentBaseStateAttackLayer.nameHash;
		if (col.gameObject.tag == "Chain" || col.gameObject.tag == "Enemy" ) {
				if (canAddDemage == true){
					if (MecanimState == a1State) {
							Debug.Log ("DemageA1");
							col.SendMessage ("Demage", 2f);
							canAddDemage = false;
							// emitt particles on hit, work though chainControl and Demage method
							//col.gameObject.GetComponentInChildren<ChainHitSc> ().SendMessage("EmittOneShot");
					
					StartCoroutine ("RestartDemage");
					} else if (MecanimState == a2State) {
							Debug.Log ("DemageA2");
							col.SendMessage ("Demage", 3f);
							canAddDemage = false;
							StartCoroutine ("RestartDemage");
					} else if (MecanimState == a3State) {
							Debug.Log ("DemageA3");
							col.SendMessage ("Demage", 5f);
							canAddDemage = false;
							StartCoroutine ("RestartDemage");
					} else if (MecanimState == a4ShB) {
							Debug.Log ("DemageA4ShB");
							col.SendMessage ("Demage", 1f);
							canAddDemage = false;
							StartCoroutine ("RestartDemage");
					}
					
				}
			}


	}

	void GetDemageForChain()
	{

	}
	IEnumerator RestartDemage()
	{
		yield return new WaitForSeconds(animationAttackTime);
		canAddDemage = true;
		StopAllCoroutines();

	}

}
