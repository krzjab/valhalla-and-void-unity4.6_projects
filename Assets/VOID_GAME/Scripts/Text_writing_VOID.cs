﻿using UnityEngine;
using System.Collections;
[RequireComponent (typeof (GUIText))]
public class Text_writing_VOID : MonoBehaviour {
	/*
	 * Goal of the script: cooperation wtih "StoryTelling" script to dispaly text and play lector audio file simultaneusly.
	 */ 

	public string textLector; // text of the story, to display
	public string textView; // controls the state of the displayed text
	public float scrollingSpeed = 5.0f; // refers to the speed of displaying text characters
	//public bool textIsScrolling; 
	bool stopText = false; //informs when all text is displayed //informs if the TextScrolling and Lector methods can be restarted
						
	//GUIStyle to Label
	public GUIStyle styleLabel; // determines text parameters (set in inspector)
	
	// Use this for initialization
	void Start () {
		// "\n" marks passage to the next line, font size 28
		textLector = "    Now Bardi and his\nbrethren had on hand\nmuch wright's work that\nsummer, and the work\nwent well the summer\nthrough, whereas it was\nbetter ordered than\nheretofore.\n    Now summer had worn\nso far that but six weeks\nthereof were left. Then fares\nBardi to Lechmote to meet\nThorarin his fosterer; often\nthey talked together privily\na long while, and men knew\nnot clearly what they said.";
		StartCoroutine (TextScrolling ());
	}
	
	// Update is called once per frame
	void Update () {
		//Restarts text scrolling and Lector methods when LMB is clicked
		if (Input.GetButtonDown ("Fire1") && stopText) {
			stopText = false;
			textView = ""; // erases the displayed text
			StartCoroutine (TextScrolling ());
			gameObject.SendMessage("Lector", 2.0f);
		}
	}
	void OnGUI() {
		// Determines the text box dimensions on the screen
		GUI.Label(new Rect(674, Screen.height - (708), 295, 708+20), textView, styleLabel);
	}
	// Displays the text on the screen
	IEnumerator TextScrolling(){
		yield return new WaitForSeconds (1.5f);
		for(int i = 0; i < textLector.Length; i++){
				textView += textLector[i];
				yield return new WaitForSeconds(scrollingSpeed /100 );
		}
		stopText = true;
	}
}
