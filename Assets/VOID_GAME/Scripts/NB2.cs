﻿using UnityEngine;
using System.Collections;

public class NB2 : MonoBehaviour {

	/*
	 * Goal of this script is to control the "Next2" button functions and GUI.
	 */ 
	
	

	public AudioClip buttonClip; // sound file of clicked button
	
	// Textures of the GUI button in normal, and rollOver state
	public Texture2D rollOverTexture;
	public Texture2D normalTexture;
	public Texture2D mouseUpTexture;
	
	//Buttons activ / disabled
	public bool buttonActive = true;
	
	// Initialization
	void Start () {
		
	}
	
	void OnMouseEnter(){
		// changes button texture when mouse cursor is on the button
		if ( buttonActive ) 
		{
			guiTexture.texture = rollOverTexture;
		}
	}
	
	void OnMouseExit(){
		// changes the texture when mouse cursor exits the button
		if (buttonActive)
		{
			guiTexture.texture = normalTexture;
		}
	}
	
	IEnumerator OnMouseUp(){


		//buttonActive = false;
		GameObject.Find ("SoundEffectPlayer").SendMessage ("Sound1");
		GameObject.Find ("background_white").SendMessage ("StartAnimation");
		GameObject.Find ("MusicPlayer").SendMessage ("EndMusic");
		// GUI changes in the scene
		GameObject.Find ("restart_button2").SetActive (false);
		GameObject.Find ("Movie1").SetActive(false);
		GameObject.Find ("background_black").SendMessage ("SetZ");
		gameObject.GetComponent<GUITexture> ().guiTexture.enabled = false;
		guiTexture.texture = null;
		// GUI changes
		GameObject.Find ("restart_button3").GetComponent<GUITexture> ().guiTexture.enabled = true;
		GameObject.Find ("next_button3").GetComponent<GUITexture> ().guiTexture.enabled = true;
		//end gui changes
		yield return new WaitForSeconds (0.01f);
		GameObject.Find ("MusicPlayer").SendMessage ("StartMusic3");
		yield return new WaitForSeconds (3f);
		GameObject.Find ("restart_button3").SendMessage ("StartTextLector");
		yield return new WaitForSeconds (1f);
		GameObject.Find ("restart_button3").SendMessage ("StartTextScrolling");

		
	}




}
