﻿using UnityEngine;
using System.Collections;

public class EndPointSc : MonoBehaviour {

	public string addMessageToFront;
	public string addMessageToGeneral;
	public bool checkPointActive;
	
	// Use this for initialization
	void Start () {
		checkPointActive = true;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void OnTriggerEnter(Collider col) {
		if (col.gameObject.tag == "Player" && checkPointActive == true) 
		{
			UpdateVoiceMessage();
			checkPointActive = false;
		}
	}
	
	void UpdateVoiceMessage(){
		GameObject.Find("InfoFront").GetComponent<guiTextBox>().textView = "Voice: " + this.addMessageToFront;
		GameObject.Find("InfoBackgroundGeneral").GetComponent<guiTextBox>().textView = this.addMessageToGeneral;
	}

}
