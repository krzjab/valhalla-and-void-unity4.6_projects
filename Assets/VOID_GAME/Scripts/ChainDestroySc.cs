﻿using UnityEngine;
using System.Collections;

public class ChainDestroySc : MonoBehaviour {

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void EmittON()
	{
		gameObject.particleEmitter.emit = true;
	}
	void EmittOFF()
	{
		gameObject.particleEmitter.emit = false;
		
	}
	IEnumerator EmittOneShot()
	{
		EmittON ();
		yield return new WaitForSeconds(3f);
		EmittOFF ();
	}


}
