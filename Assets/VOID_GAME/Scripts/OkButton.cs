﻿using UnityEngine;
using System.Collections;

public class OkButton : MonoBehaviour {
	/*
	 * Goal of this script is to control the "OkButton" button functions and GUI.
	 * It referes to closeing ControlInfo GUI.
	 */ 
	public AudioClip buttonClip; // sound file of clicked button
	
	// Textures of the GUI button in normal, and rollOver state
	public Texture2D rollOverTexture;
	public Texture2D normalTexture;
	public Texture2D mouseUpTexture;
	private GUITexture[] guis;
	//Buttons activ / disabled
	public bool buttonActive = true;
	
	// Initialization
	void Start () {
		
	}
	
	void OnMouseEnter(){
		// changes button texture when mouse cursor is on the button
		if ( buttonActive ) 
		{
			guiTexture.texture = rollOverTexture;
		}
	}
	
	void OnMouseExit(){
		// changes the texture when mouse cursor exits the button
		if (buttonActive)
		{
			guiTexture.texture = normalTexture;
		}
	}
	
	IEnumerator OnMouseUp(){
		
		
		//buttonActive = false;
	


		// GUI changes in the scene


	
	
		// GUI changes
	
		//end gui changes

		GameObject.Find ("SoundEffectPlayer").SendMessage ("PaperSound2");
		yield return new WaitForSeconds(1);
		gameObject.guiTexture.enabled = true;
		guis = gameObject.GetComponentsInParent<GUITexture>();
		foreach (GUITexture g in guis) {
			g.enabled = false;
		}

		
	}
}
