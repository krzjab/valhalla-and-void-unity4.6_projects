﻿using UnityEngine;
using System.Collections;

public class FLF_Sc : MonoBehaviour {

	public ParticleEmitter[] emitters;
	public float lifeEssence;
	public float lifeEssenceMax;
	public float regDem; // to change the darknes life Energy regeneration demage
	public GameObject LifeBar;

	// Use this for initialization
	void Start () {
		lifeEssence = 20;
		lifeEssenceMax = 20;
		regDem = 2;
		LifeBar = GameObject.Find ("LifeBar");
	}
	
	// Update is called once per frame
	void Update () {
		LoseLifeEssence ();
		LifeBar.SendMessage ("ReduceBar", lifeEssence / lifeEssenceMax);
	}
	void FlamesOn()
	{
		emitters = gameObject.GetComponentsInChildren<ParticleEmitter> ();
		foreach (ParticleEmitter pe in emitters)
		{
			pe.emit = true;
		}
		gameObject.GetComponentInChildren<Light> ().light.enabled = true;

	}
	void FlamesOff()
	{
		emitters = gameObject.GetComponentsInChildren<ParticleEmitter> ();
		foreach (ParticleEmitter pe in emitters)
		{
			pe.emit = false;
		}
		gameObject.GetComponentInChildren<Light> ().light.enabled = true;
		gameObject.GetComponentInChildren<Light> ().intensity = Mathf.Lerp (1.15f, 0.2f, 5f);
	}

	void OnTriggerStay(Collider col)
	{	
		

		if (col.gameObject.tag == "Player" ) {
			//energia rośnie
			if(this.lifeEssence >= this.lifeEssenceMax)
			{
				this.lifeEssence = this.lifeEssenceMax;
			} else if( this.lifeEssence < this.lifeEssenceMax)
			{
				this.lifeEssence = this.lifeEssence + Time.deltaTime*(this.regDem);
			}


		}
	}
	void LoseLifeEssence()
	{
		if(this.lifeEssence <= 0f)
		{
			this.lifeEssence = 0f;
			Application.LoadLevel(2);
		} else if( this.lifeEssence > 0f)
		{
			this.lifeEssence = this.lifeEssence - Time.deltaTime;
		}
	}

	void AddDemage(float demage)
	{
		lifeEssence = lifeEssence - demage;
	}
}
