﻿using UnityEngine;
using System.Collections;

public class ControlsInfo : MonoBehaviour {


	private GUITexture[] guis;
	// Use this for initialization
	void Start () {
		StartCoroutine ("Begin");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	IEnumerator Begin()
	{
		yield return new WaitForSeconds(2);
		GameObject.Find ("SoundEffectPlayer").SendMessage ("PaperSound1");
		gameObject.guiTexture.enabled = true;
		guis = gameObject.GetComponentsInChildren<GUITexture>();
		foreach (GUITexture g in guis) {
			g.enabled = true;
		}
	}

}
