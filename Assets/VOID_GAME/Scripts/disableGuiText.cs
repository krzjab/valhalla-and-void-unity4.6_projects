﻿using UnityEngine;
using System.Collections;

public class disableGuiText : MonoBehaviour {
	private float timeToEndBlackFadeOut;
	// Use this for initialization
	void Start () {
		this.timeToEndBlackFadeOut = 5f;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	IEnumerator DisableGuiText()
	{
		GameObject.Find ("background_text").GetComponent<GUIText> ().enabled = false;
		yield return new WaitForSeconds (timeToEndBlackFadeOut);
		GameObject.Find ("Text").SendMessage ("EnableTextAnimation");
	}

}
