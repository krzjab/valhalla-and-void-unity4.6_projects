﻿using UnityEngine;
using System.Collections;

public class HelpButton : MonoBehaviour {
	/*
	 * Goal of this script is to display "ControlInfo" and "OkButton GUI
	 */ 
	public AudioClip buttonClip; // sound file of clicked button
	
	// Textures of the GUI button in normal, and rollOver state
	public Texture2D rollOverTexture;
	public Texture2D normalTexture;
	public Texture2D mouseUpTexture;
	private GUITexture[] guis;
	//Buttons activ / disabled
	public bool buttonActive = true;
	
	// Initialization
	void Start () {
		StartCoroutine (Display (2.0f)); //on game start view the Help screen
	}
	
	void OnMouseEnter(){
		// changes button texture when mouse cursor is on the button
		if ( buttonActive ) 
		{
			guiTexture.texture = rollOverTexture;
		}
	}
	
	void OnMouseExit(){
		// changes the texture when mouse cursor exits the button
		if (buttonActive)
		{
			guiTexture.texture = normalTexture;
		}
	}
	
	void OnMouseUp(){
		
		
		//buttonActive = false;
		
		
		
		// GUI changes in the scene
		
		
		
		
		// GUI changes
		
		//end gui changes
		
		StartCoroutine ("Display");
		
		
	}
	void Display()
	{

		GameObject.Find ("SoundEffectPlayer").SendMessage ("PaperSound1");

		guis = GameObject.Find ("ControlsInfo").GetComponentsInChildren<GUITexture>();
		foreach (GUITexture g in guis) {
			g.enabled = true;
		}
	}
	IEnumerator Display( float waitTime)
	{
		yield return new WaitForSeconds (waitTime);
		Display ();
	}
}
