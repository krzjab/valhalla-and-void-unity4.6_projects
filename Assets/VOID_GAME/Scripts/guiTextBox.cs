﻿using UnityEngine;
using System.Collections;

public class guiTextBox : MonoBehaviour {

	//GUIStyle to Label
	public GUIStyle styleLabel; // determines text parameters (set in inspector)

	public string textView; // controls the state of the displayed text

	public int infoNumber; // to control viewed infoX string

	public string info1 ;
	public string info2 ;
	public string info3 ;
	public string info4 ;
	public string info5 ;
	public string info6 ;
	public string info7 ;
	public string info8 ;
	public string info9 ;
	public string info10 ;
	public string info11 ;

	public bool guiTextLabelON; // to control GUILabel text view'ing

	private float X;
	private float Y;
	private float W;
	private float H;

	private float basseGuiWidth;
	private float currenGuiWidth;
	// Use this for initialization
	void Start () {
		GetGuiData ();
		basseGuiWidth = guiTexture.pixelInset.width;
		currenGuiWidth = guiTexture.pixelInset.width;
		guiTextLabelON = true;
	}
	
	// Update is called once per frame
	void Update () {
		ChangeInfo ();
	}

	void GetGuiData()
	{
		styleLabel.wordWrap = true;
		X = guiTexture.pixelInset.x;
		Y = guiTexture.pixelInset.y;
		W = guiTexture.pixelInset.width;
		H = guiTexture.pixelInset.height;
	}

	void OnGUI() {
		if (guiTextLabelON == true)
		// Determines the text box dimensions on the screen
		GUI.Label(new Rect(0 +X, Screen.height - H-Y, W,H ), textView, styleLabel);
	}
	// Give ther current amount, and the max amount X/Xmax
	// Function callculate the width of the color bar
	void ReduceBar(float percent)
	{
		this.currenGuiWidth = basseGuiWidth * percent;
		//Debug.Log ("current gui width is: " + this.currenGuiWidth);
		if (this.currenGuiWidth <= 0)
						this.currenGuiWidth = 0;
		guiTexture.pixelInset = new Rect (guiTexture.pixelInset.x, guiTexture.pixelInset.y, currenGuiWidth, guiTexture.pixelInset.height );
	}
	void ChangeInfo()
	{
		switch (infoNumber) 
		{
		case 1: 
			this.textView = this.info1;
			break;
		case 2: 
			this.textView = this.info2;
			break;
		case 3:
			this.textView = this.info3;
			break;
		case 4:
			this.textView = this.info4;
			break;
		case 5:
			this.textView = this.info5;
			break;
		case 6: 
			this.textView = this.info6;
			break;
		case 7: 
			this.textView = this.info7;
			break;
		case 8:
			this.textView = this.info8;
			break;
		case 9:
			this.textView = this.info9;
			break;
		case 10:
			this.textView = this.info10;
			break;
		case 11:
			this.textView = this.info11;
			break;
		}
	}

	void GuiViewON()
	{
		gameObject.GetComponent<GUITexture> ().enabled = true;
		guiTextLabelON = true;
	}
	void GuiViewOFF()
	{
		gameObject.GetComponent<GUITexture> ().enabled = false;
		guiTextLabelON = false;
	}


}
