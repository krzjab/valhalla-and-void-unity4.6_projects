﻿using UnityEngine;
using System.Collections;

public class LightAnimControl : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void DisableAnimation()
	{
		gameObject.GetComponent<Animator> ().enabled = false;
	}
	void CheckpointSound()
	{
		GameObject.Find ("SoundEffectPlayer").SendMessage("CheckpointSound"); // emit chain hit sound fx
	}
}
