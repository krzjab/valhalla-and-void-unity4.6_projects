﻿using UnityEngine;
using System.Collections;
[RequireComponent (typeof (GUIText))]
public class RB3_TW : MonoBehaviour {
	/*
	 * Goal of the script: cooperation wtih "RB3_ST" script to dispaly text and play lector audio file simultaneusly.
	 */ 
	
	public string textLector; // text of the story, to display
	public string textView; // controls the state of the displayed text
	public float scrollingSpeed = 10.0f; // refers to the speed of displaying text characters
	//public bool textIsScrolling; 
	bool stopText = false; //informs when all text is displayed //informs if the TextScrolling and Lector methods can be restarted
	
	//GUIStyle to Label
	public GUIStyle styleLabel; // determines text parameters (set in inspector)
	
	// Use this for initialization
	void Start () {
		textLector = "Suddenly a beam of light falls down upon you. Something has changed. \nYou can not see nothing but still, you sense a presence. And... that voice...";
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	//Restart text scrolling and Lector methods when Button is clicked
	void OnMouseUp(){
		
		if(stopText) 
		{
			stopText = false;
			textView = ""; // erases the displayed text
			StartCoroutine (TextScrolling ());
			gameObject.SendMessage("Lector", 2.0f);
		}
		
	}
	
	
	void OnGUI() {
		// Determines the text box dimensions on the screen
		GUI.Label(new Rect(25, Screen.height-180, 900, 160), textView, styleLabel);
	}
	// Displays the text on the screen
	IEnumerator TextScrolling(){
		yield return new WaitForSeconds (1.5f);
		for(int i = 0; i < textLector.Length; i++){
			textView += textLector[i];
			yield return new WaitForSeconds(scrollingSpeed /100 );
		}
		stopText = true;
	}
	public void StartTextScrolling()
	{
		// "\n" marks passage to the next line, font size 28
		textLector = "Suddenly a beam of light falls down upon you. Something has changed. \nYou can not see nothing but still, you sense a presence. And... that voice...";
		StartCoroutine (TextScrolling ());
	}
}
