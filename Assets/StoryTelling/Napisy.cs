﻿using UnityEngine;
using System.Collections;
[RequireComponent (typeof (GUIText))]

public class Napisy : MonoBehaviour {

	public string tekstLektora;
	public string tekstWyswietla;
	public float przewijanie = 5.0f;
	public bool textIsScrolling;
	bool stopowanie = false;

	//GUIStyle do Label
	public GUIStyle stylLabel;



	// Use this for initialization
	void Start () {
		// "\n" oznacza przejscie do nastepnej linii
		// wersja pierwsza :) czcionka 34
		//tekstLektora = "  In a faraway land\n where dogs bark with\n their asses and shit\n with their mouths -\n there was a small\n village in the woods\n and in there a tiny\n little boy.\n  No one could ever\n predict what will\n happen to him or his\n brethren.";
		// wersja 2, czcionka 28
		//tekstLektora = "    Now Bardi and his\nbrethren had on hand\nmuch wright's work that\nsummer, and the work\nwent well the summer\nthrough, whereas it was\nbetter ordered than\nheretofore.\n    Now summer had worn\nso far that but six weeks\nthereof were left. Then fares\nBardi to Lechmote to meet\nThorarin his fosterer; often\nthey talked together privily\na long while, and men knew\nnot clearly what they said.";
		//wersja 3, czcionka ?
		tekstLektora = "  Now Odd turns to Uspak, and bids him take over to him the household. Uspak answers: u That would be over-much for me, how well soever things go, now thou hast to do therewith. Odd urges the matter, and Uspak excuses himself, as sorely as he desired to take it; so at last it came to this, that he bade Odd have his way, if he would promise him his help and furtherance. Odd says that he shall so deal with his possessions that he may wax the better man thereby, and be more highly favoured, and that he had put it to the proof that no man either could or would watch better over his wealth. Uspak bids him now to do according to his will, and so the talk ended.  ";
		StartCoroutine (ScrollowanieNapisu ());
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown ("Fire1") && stopowanie) {
			stopowanie = false;
			tekstWyswietla = ""; // usuwam napis i leci od nowa
			StartCoroutine (ScrollowanieNapisu ());
			GameObject.Find ("button").SendMessage("Lector", 2.0f);
				}

				

	
	}

	void OnGUI() {
		//GUI.Label(new Rect(674+10, 30+10, 295, 708), tekstWyswietla, stylLabel);
		GUI.Label(new Rect(674, Screen.height - (708), 295, 708+20), tekstWyswietla, stylLabel);
	}

	
	IEnumerator ScrollowanieNapisu(){
		yield return new WaitForSeconds (1.5f);
		bool tekstIsScrolling = true;
		for(int i = 0; i < tekstLektora.Length; i++){
			if(tekstIsScrolling){
				tekstWyswietla += tekstLektora[i];
				// -------------------------- dziala ale zamknalem aby zamiast tego dac do GUI.Label
			// guiText.text = tekstWyswietla;
				// -------------------------- koniec ( dziala ale zamknalem aby zamiast tego dac do GUI.Label)
				yield return new WaitForSeconds(przewijanie /100 );
			}
		}
		stopowanie = true;
	}



}
