﻿using UnityEngine;
using System.Collections;
[RequireComponent (typeof(AudioSource))]

public class StoryTelling : MonoBehaviour {
	/*
	 * Goal of the script: cooperation wtih "Text_writing" script to dispaly text and play lector audio file simultaneusly.
	 * Load the next level of the game by a button interaction. 
	 */ 

	public string levelToLoad; // which level to load 
	public AudioClip lectors; // lectors audio file 
	public AudioClip music; // the background music file 
	public AudioClip buttonClip; // sound file of clicked button

	// Textures of the GUI button in normal, and rollOver state
	public Texture2D rollOverTexture;
	public Texture2D normalTexture;

	// Initialization
	void Start () {
		// audio refers to AudioSource component of this obiekt,
		audio.playOnAwake = true;
		audio.loop = true;
		audio.clip = music;
		audio.Play();
		// activates Lector method, wait 2 sec.
		StartCoroutine (Lector(2.0f));
	}

	void OnMouseEnter(){
		// changes button texture when mouse cursor is on the button
		guiTexture.texture = rollOverTexture;
	}
	
	void OnMouseExit(){
		// changes the texture when mouse cursor exits the button
		guiTexture.texture = normalTexture;
	}

	IEnumerator OnMouseUp(){
		// Loading next level and giving a sound signal when LMB is clicked over the button.
		audio.PlayOneShot(buttonClip);
		yield return new WaitForSeconds (0.6f);
		Application.LoadLevel (levelToLoad);
	}
	// Plays the lector music file
	IEnumerator Lector(float wait){
		yield return new WaitForSeconds (wait);
		audio.PlayOneShot(lectors);
	}
}
