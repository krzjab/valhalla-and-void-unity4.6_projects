﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GateScript : MonoBehaviour {

	Animator anim;


	public float souls;
	public float soulsEnough;

	Text GameInformer2;


	// Use this for initialization
	void Start () {
		GameInformer2 = GameObject.FindWithTag("InfoManager").GetComponent<Text>();
		anim = 	GetComponentInParent<Animator>();
		souls = GameObject.FindWithTag ("ScoreMenager").GetComponent<ScoreMenager>().score;
		soulsEnough = 500f;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	IEnumerator OnTriggerEnter(Collider col)
	{
		souls = GameObject.FindWithTag ("ScoreMenager").GetComponent<ScoreMenager> ().score;
		if ((souls >= soulsEnough) && (col.gameObject.tag == "Player")) {

			anim.SetBool("GateOpen", true);
			yield return new WaitForSeconds(0.0f);

		} else {
			GameInformer2.enabled = true;
			GameInformer2.text = "U need " + soulsEnough+ " " + "souls to open the Gate" ;
			yield return new WaitForSeconds(3.0f);
			GameInformer2.enabled = false;
		}
	}

}
