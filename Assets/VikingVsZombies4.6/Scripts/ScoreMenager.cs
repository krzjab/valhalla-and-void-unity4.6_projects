﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreMenager : MonoBehaviour {

	public float score; // players score

	public Text textUI; // reference to text score



	void Awake()
	{
		// set up the reference
		textUI = GetComponent<Text> ();

		//reset the score
		score = 0;
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		textUI.text = "Score: " + score + "\n" +"\nSouls: " + score;
	
	}

	void AddPoints(float value)
	{
		score += value;
	}
}
