﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

//add this ecript to end game colider
public class EndGameScr : MonoBehaviour {

	public Sprite[] images;
	public string[] storyTexts;
	public string[] buttonTexts;
	
	public Image currentImage;
	public Text currentButtonText;
	public Text currentText;

	public ScoreMenager ScoreManager;
	public TimeMenager TimeManager;

	public int gateNumbers;  //how much gates u have opened
	public float leadedSouls; // savedSouls
	public float extraTimeForSouls; // 300/6 = 50

	public Image[] endCanvasImgs;
	public Text[] endCanvasTexts;

	void EndCanvasON() {
		endCanvasImgs = GameObject.Find("CanvasEndGame").GetComponentsInChildren<Image>();
		endCanvasTexts = GameObject.Find("CanvasEndGame").GetComponentsInChildren<Text>();
		foreach (Image img in endCanvasImgs) {
			img.enabled = true;
		}
		foreach (Text tx in endCanvasTexts) {
			tx.enabled = true;
		}
	}
	
	void EndCanvasOFF() {
		endCanvasImgs = GameObject.Find("CanvasEndGame").GetComponentsInChildren<Image>();
		endCanvasTexts = GameObject.Find("CanvasEndGame").GetComponentsInChildren<Text>();
		foreach (Image img in endCanvasImgs) {
			img.enabled = false;
		}
		foreach (Text tx in endCanvasTexts) {
			tx.enabled = false;
		}
	}

	void DisableCanvas()
	{
		GameObject.Find ("CanvasHealthBar").SetActive (false);
		GameObject.Find ("CanvasScore").SetActive (false);
		GameObject.Find ("CanvasTimer").SetActive (false);

	}
	void Awake()
	{
		EndCanvasOFF ();
	}

	// Use this for initialization
	void Start () {

		gateNumbers = 0;

		currentImage = GameObject.Find ("EndGameImg").GetComponent<Image> ();
		currentText = GameObject.Find ("EndGameText").GetComponent<Text> ();
		currentButtonText = GameObject.Find ("EndGameText1").GetComponent<Text> (); // restart button, czy wygrana, czy gameOver

		ScoreManager = GameObject.FindWithTag ("ScoreMenager").GetComponent<ScoreMenager> ();
		TimeManager = GameObject.FindWithTag ("TimeMenager").GetComponent<TimeMenager> ();
		
		


		//EndCanvasOFF ();

	}
	
	// Update is called once per frame
	void Update () {

		if (TimeManager.TimeInt < 0) {
			EndGameFunctionBad();
		}

	}


	void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.tag == "Player") {
			EndGameFunctionGood ();
		}
	}

	void GetEndGameScore()
	{
		extraTimeForSouls = TimeManager.TimeInt;
		if (TimeManager.TimeInt > 0) {
			leadedSouls = ScoreManager.score + extraTimeForSouls / 6 - (extraTimeForSouls / 6)%1;
		} else {
			leadedSouls = ScoreManager.score;
		}
		storyTexts [0] = "GAME OVER - Well done.\nYou opened all gates and leaded " +leadedSouls+ " souls.\nSalvatore awaits You...\nExtraTime/6 = 1 Soul\nScore: " + ScoreManager.score+ "\nExtra Time: " + extraTimeForSouls +"\n[The Battle of Clontarf, oil on canvas painting by Hugh Frazer, 1826, Isaacs Art Center, public domain.]";
		storyTexts [1] = "GAME OVER.\nYour remains changed to a fallen guardian... Butt you managed to open " + gateNumbers + " gates " + "and lead " + leadedSouls + " souls on your path to redemption. Have hope... there will come a ultimate warrior to redeem all...\nExtraTime/6 = 1 Soul\n[\"Asgardsreien\" (1872), Norse mythology brought to life by Peter Nicolai Arbo]";
		
		buttonTexts [0] = "Not enough? Push up the score";
		buttonTexts [1] = "Next Warrior";

		EndCanvasON ();
		DisableCanvas ();
	}

	void EndGameFunctionGood()
	{
		GetEndGameScore ();

		GameObject.Find ("CanvasEndGame").SetActive (true);
		currentImage.sprite = images [0];
		currentText.text = storyTexts [0];
		currentButtonText.text = buttonTexts [0];
	}
	void EndGameFunctionBad()
	{
		GetEndGameScore ();

		GameObject.Find ("CanvasEndGame").SetActive (true);
		currentImage.sprite = images [1];
		currentText.text = storyTexts [1];
		currentButtonText.text = buttonTexts [1];
	}
	void ExitGame()
	{
		Application.Quit ();

	}
	void RestartGame()
	{
		Application.LoadLevel(1);

	}

}



