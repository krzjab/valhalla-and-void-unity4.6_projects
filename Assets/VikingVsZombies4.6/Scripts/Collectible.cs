﻿using UnityEngine;
using System.Collections;

public class Collectible : MonoBehaviour {

	Animator anim;



	// Use this for initialization
	void Start () {
		anim = gameObject.GetComponentInChildren<Animator> ();

	}
	
	// Update is called once per frame
	void Update () {

		AnimationRandomizer ();
	}

	void OnTriggerEnter(Collider col)
	{

		if (col.gameObject.tag == "Player"  ) {

			GameObject.FindWithTag("ScoreMenager").SendMessage("AddPoints", 10.0f);
			GameObject.Find("SoundEffectPlayer").SendMessage("PickUp");
			Destroy(gameObject);

		}

	}

	void AnimationRandomizer()
	{	
		int randomNumber = Random.Range (0, 11);

		anim.SetInteger ("Random", randomNumber - randomNumber % 1);
	}

}
