﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HealthBarScr : MonoBehaviour {


	public Scrollbar Healthbar;
	public float Health = 100;

	public bool gameOverBad = false;

	public void Damage(float value)
	{
		Health -= value;
		Healthbar.size = Health / 100f;
	}
	// Use this for initialization
	void Start () {
		gameOverBad = false;
	}
	
	// Update is called once per frame
	void Update () {

	}


	//Usmiercam gracza tym kodem w scrypcie PlayerStatus.sc
	void EndGamePlayerDead()
	{
		if (Health <= 0  && gameOverBad == false) {
			gameOverBad = true;
			GameObject.Find("EndGameButton").SendMessage("EndGameFunctionBad");


		}
	}
}
