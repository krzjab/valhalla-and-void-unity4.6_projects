﻿using UnityEngine;
using System.Collections;

public class RayCastDestructible : MonoBehaviour {

	RaycastHit hit;
	GameObject hitObject ;
	


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		//Raycasting nie działa na triger, raycasting działa na colider
		
		if (Physics.Raycast(transform.position, transform.forward, out hit, 1)) 
		{
			if(hit.collider.gameObject.tag=="Enemy" && GameObject.Find("Healthbar").GetComponent<GUITexture>().enabled == false)
			{	
				//reamember colliders must not cross eachother -->> bugs

				//get objectName
				GameObject.Find("Healthbar").GetComponent<guiTextBox>().textView = "Fallen guardian";

				hit.collider.gameObject.SendMessage("ChainInfo");
				// get Chain hp and Update Healthbar
				
				hit.collider.gameObject.GetComponent<Destructible>().GuiUpdate();
				
				// end
				GameObject.Find("Healthbar").GetComponent<GUITexture>().enabled = true;
				GameObject.Find("Healthbar").GetComponent<guiTextBox>().enabled = true;
				GameObject.Find("HealthbarBack").GetComponent<GUITexture>().enabled = true;
				Debug.Log ("ON raycasting");
				//Debug.Log("POBIERASZ informacje z chaina");
				
			}else{
				
			}
		} else{
			if(GameObject.Find("Healthbar").GetComponent<GUITexture>().enabled == true){
				GameObject.Find("Healthbar").GetComponent<GUITexture>().enabled = false;
				GameObject.Find("Healthbar").GetComponent<guiTextBox>().enabled = false;
				GameObject.Find("HealthbarBack").GetComponent<GUITexture>().enabled = false;
				Debug.Log ("OFF Raycastingu");
			}
		}
	}
}

