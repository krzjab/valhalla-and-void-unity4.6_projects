﻿
using UnityEngine;
using System.Collections;

public class Destructible : MonoBehaviour {


	private float chainHP;
	private float chainHPmax;

	
	// Use this for initialization
	void Start () {
		chainHP = 10f;
		chainHPmax = 10f;
		
	}
	
	// Update is called once per frame
	void Update () {
		
		
		
	}
	
	void ChainInfo()
	{
		//Debug.Log ("Pobierasz informacje z chaina");
	}
	void Demage(float demage)
	{
		this.chainHP = this.chainHP - demage;
		Debug.Log ("HP level is:" + chainHP);
		GuiUpdate();
		ParticleHitEffect ();
		GameObject.Find ("SoundEffectPlayer").SendMessage("ChainHitSound"); // emit chain hit sound fx
		if (this.chainHP <= 0) {
			ParticleDestroyEffect ();
			StartCoroutine("WaitAndDestroyChain");
			//set barierParent demage method --->>>>

			
		}
	}
	public void GuiUpdate()
	{
		GameObject.Find("Healthbar").SendMessage("ReduceBar", this.chainHP/this.chainHPmax);
	}
	
	void ParticleHitEffect()
	{
		gameObject.GetComponentInChildren<ChainHitSc>().SendMessage("EmittOneShot");
		
	}
	void ParticleDestroyEffect()
	{
		gameObject.GetComponentInChildren<ChainDestroySc>().SendMessage ("EmittOneShot");
	}
	IEnumerator WaitAndDestroyChain()
	{

		gameObject.SendMessage("DeactivateAI");
		GameObject.FindWithTag("ScoreMenager").SendMessage("AddPoints", 10.0f); //send message to add points for killing the monster

		GameObject.Find ("SoundEffectPlayer").SendMessage("ChainDestroySound"); // emit chain destroy sound fx
		Debug.Log ("Teraz powinna zmienić się informacja na komunikacie wyświetlanym  na InfoFront");
		// deactivate chainParent
		yield return new WaitForSeconds (1f);


		gameObject.SetActive(false);

	}
	

	
	public float GetChainHP()
	{
		return chainHP;
	}
	public float GetChainHPmax()
	{
		return chainHPmax ;
	}
}


	
	
	
	

