﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StoryTellingScr : MonoBehaviour {

	public Sprite[] images;
	public string[] storyTexts;

	public Image currentImage;
	public Text currentText;

	public int numberOfIT;







	// Use this for initialization
	void Start () {
		currentImage = GameObject.Find ("Image").GetComponent<Image> ();
		currentText = GameObject.Find ("ImgText").GetComponent<Text> ();

		numberOfIT = 0;


		storyTexts [0] = "History of mankind was full of wars and bloodshed. Many warriors have fallen in battle.\n\n Some of them were pierced with arrows.";


		storyTexts [1] = "Many fighters were killed of after great battles.  ";
		storyTexts [2] = "You were one of them. You were pierced with a spear while laying in your own blood.  ";
		storyTexts [3] = "No honor in this. Your life came to an end.\n\nYour kind talked about a place called VALHALLA. A place of eternal feasts and battles. Your hope...   ";
		storyTexts [4] = "You awaken in a strange place. What monstrosity was that? Place full of unknown creatures... hideous things... if only your folks could see that?\n\nBattle is all you can get here... for eternity.  ";
		storyTexts [5] = "\"My name is Salvatore and I'll be your guide in this place.\" - he becomes sillent - \"Many warriors get here. They come to this place because all they have done, while alive, was fighting, killing and going to battle.\" - he looks strait into your eyes.\n\"You have to make a decision...\"  ";
		storyTexts [6] = "\"You have to make a decision... You can fight all eternity with the fallen guardians and eventually become one of those creatures...\"  ";
		storyTexts [7] = "\"... or you can redeem yourself, your folk and go with me to the place you all call Valhalla.\"  ";
		storyTexts [8] = "Game mechanics tips: \"Avoid those corrupted spirits as you can. Pickup up all redemptive souls, destroy ale soul vertex's you can see and find the exit from the labyrinth. Gates will open only if you manage to collect enough souls.\"";
		storyTexts [9] = " \"Hopefully will be seeing each other on a long lasting feast\" - he smiles softly and disappears.\n\n Now help yourself and your brothers in need... ";



		currentImage.sprite = images [numberOfIT];
		currentText.text = storyTexts [numberOfIT];
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void ChangeImgState()
	{
		numberOfIT += 1;

		if (numberOfIT < images.Length) {
			currentImage.sprite = images [numberOfIT];
			currentText.text = storyTexts [numberOfIT];

		} else {
			Application.LoadLevel(1);

		}
	}
	void BackImage()
	{	
		if (numberOfIT > 0) {
			numberOfIT -= 1;
			currentImage.sprite = images [numberOfIT];
			currentText.text = storyTexts [numberOfIT];
		}
	}

}
