﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerStatus : MonoBehaviour {
	public int health = 100;
	public int healthLimit = 100;

	public Scrollbar Healthbar;

	void Awake()
	{
		Healthbar = GameObject.FindWithTag("HealthBar").GetComponent<Scrollbar>();
		health = 100;
		healthLimit = 100;
	}

	void AddHealth(int increase)
	{
		health += increase;
		
		if(health > healthLimit)
		{
			health = healthLimit;
		}
		//updathe the healthbar
		Healthbar.size = health / 100f;
	}
	/*
	void AddDamage(int damage)
	{
		health -= damage;
		if(health <= 0)
		{
			health = 0;
			Die();
		}
		//updathe the healthbar
		Healthbar.size = health / 100f;
	}
*/
	IEnumerator AddDamage(int damage)
	{
		yield return new WaitForSeconds (0.0f);
		health -= damage;
		if(health <= 0)
		{
			health = 0;
			Die();
		}
		//updathe the healthbar
		Healthbar.size = health / 100f;
	}
	
	void Die()
	{
		Debug.Log("You Died");
		GameObject.Find("EndGameButton").SendMessage("EndGameFunctionBad");
	}
}
