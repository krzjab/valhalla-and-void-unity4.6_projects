﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StoryTellingScr2 : MonoBehaviour {
	
	public Sprite[] images;
	public string[] storyTexts;
	
	public Image currentImage;
	public Text currentText;
	
	public int numberOfIT;
	
	
	
	
	
	
	
	// Use this for initialization
	void Start () {
		currentImage = GameObject.Find ("Image").GetComponent<Image> ();
		currentText = GameObject.Find ("ImgText").GetComponent<Text> ();
		
		numberOfIT = 0;
		
		storyTexts [0] =  "Blood from the veins of Alfred. Ăthelflaeda (Ethelfleada) the Mercian (Middle Anglian) English warrior Princess stands over a dead Viking.\nArtist: Mark Taylor\nwww.englandandenglishhistory.com/anglo-saxons-alfred-the-great/default.html\nHistory of mankind was full of wars and bloodshed. Many warriors have fallen in battle. You were one of them. You were pierced with a spear while laying in your own blood.  ";
		storyTexts [1] = "History of mankind was full of wars and bloodshed. Many warriors have fallen in battle. You were one of them. You were pierced with a spear while laying in your own blood.  ";
		storyTexts [2] = "No honor in this. Your life came to an end. Your kind talked about a place called VALHALLA. A place of eternal feasts and battles. Your hope...   ";
		storyTexts [3] = "You awaked in a strange place. What monstrosity was that? Place full of unknown creatures... hideous things... if only your folks could see that? Battle is all you can get here... for eternity.  ";
		storyTexts [4] = "Suddenly you see a figure coming out of darknes. -\"My name is Salvatore and I'll be your guide in this place.\" - he becomes sillent - \"Many warriors get here. They come to this place because all they have done, while alive, was fighting, killing and going to battle. This is Valhalla.\"  - he looks strait into your eyes.\n\"You have to make a decision...\"  ";
		storyTexts [5] = "\"You can fight all eternity with the fallen warriors of Valhalla and eventually become one of those creatures...\"  ";
		storyTexts [6] = "\"... or you can redeem yourself, your folk and go with me to your destiny.\"  ";
		storyTexts [7] = "Game mechanics tips: \"Avoid those corrupted warriors as you can. Pick up all redemptive souls, destroy ale soul vertex's you can see and find the exit from the labyrinth. Gates will open only if you manage to collect enough souls.\"";
		storyTexts [8] = " \"Hopefully will be seeing each other on a long lasting feast\" - he smiles softly and disappears. Now help yourself and your brothers in need... ";
		
		
		
		currentImage.sprite = images [numberOfIT];
		currentText.text = storyTexts [numberOfIT];
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void ChangeImgState()
	{
		numberOfIT += 1;
		
		if (numberOfIT < images.Length) {
			currentImage.sprite = images [numberOfIT];
			currentText.text = storyTexts [numberOfIT];
			
		} else {
			Application.LoadLevel(1);
			
		}
	}
	void BackImage()
	{	
		if (numberOfIT > 0) {
			numberOfIT -= 1;
			currentImage.sprite = images [numberOfIT];
			currentText.text = storyTexts [numberOfIT];
		}
	}
	
}
