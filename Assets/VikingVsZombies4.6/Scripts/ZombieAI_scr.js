﻿#pragma strict

var theAnimator : Animator;
var charControl : CharacterController;
var walkSpeed : float = 2.0;

var target : Transform;
var alerted : boolean = false;
var snarlSound : AudioClip;
var soundReady : boolean = true;

var turnSpeed : float = 60.0;
var turning : boolean = false;
var angle : float;

//add range detection
var distance:float; // current range betwin zombie and the target/player
var awareRange: float= 4.0; // in this range zombie is being aware of player/target

//patrol behaviour
var patrolPts : Transform[];
var currPt : int;
var targetedPt : Transform;
var ptDistance : float;
var changeDistance : float = 1; //distance to change the waypoint
var turnTime : float = 5.0;
var moving : boolean = true ;
var speed : float;
var alertDistance : float = 3.0; //in this distance AI starts to forgeting players presence

//zombie attacking
var attackRange:float = 1.5;
var attacking : boolean = false;
var attackDuration:float = 2.4;
var attackTimer : float;
var attackEnabled : boolean = true; // for disabling AI to attack while partical effects are working

//modyfiing attack function
var playerDamaged : boolean = false;

//zombie alert TimeOut
var alertTimeOut : float = 5.0;
var alertTimer : float;


//zombie demage
var damage:int = 15;
var damageVoid:int = 6;

// demaging the zombie
var zombieHealth : int =5;
var deadPrefab : Transform;

//Sensors State
var sensorIn:boolean = false;

//demage ading to player, waiting time depending on the animation speed
var demageWait:float;

function Start()
{
	
	
	// demage wait
	demageWait = 1.4;
	//demage set
	damage = 10;
	
	sensorIn = false;
	
	//find the player as a target
	target = GameObject.Find("VIKING").transform;

	theAnimator = GetComponent(Animator);
	charControl =  GetComponent(CharacterController);
	
	yield WaitForSeconds (Random.Range(3, 5));
	
	//turning on start to the first target:
	turning = true;
	
	Walks();
}


		//AI sensors reset the pathfinding target
	function OnTriggerEnter(col : Collider) 
	{	
			
			
			if(col.gameObject.tag == "Border_AI")
			{
					
				alerted = false;
				sensorIn = true;
				
				alertTimer = 0.0;
				
				//get a specifik currPt
				//currPt = 0;
				
				turning = true;
				
				//Randomize Waypoints choose
				//var sizePatrolPts:int =  patrolPts.Length;
				//var randomNumber:float = Random.value;
				
				//gets your random currPt
				//currPt = (randomNumber * sizePatrolPts) - randomNumber*sizePatrolPts % 1;
				//Debug.Log(" Zombie idzie w kierunku randomowego waypointa, w tym przypadku wylosował waypoint o numerze: " + ((randomNumber * sizePatrolPts) - randomNumber*sizePatrolPts % 1) );
				
				//get your Next Patrol Point
				// Is realized by "ChangePt" Method after geting neer targeted waypoint
				
				
			}
	        
	 }
	 //AI sensor geting Out
	 function OnTriggerExit(col : Collider) 
	{	
			if(col.gameObject.tag == "Border_AI")
			{
				sensorIn = false;
			}
	      
	 }

function Update()
{




	//adjust walkingforward animation
	var speedFactor = theAnimator.GetFloat("ForwardMovement");
	speed = walkSpeed * speedFactor;

	//patrol behaviour
	targetedPt = patrolPts[currPt];
	var patrolPtDistance : float = Vector3.Distance(targetedPt.position, transform.position);
	var playerDistance = Vector3.Distance(target.position, transform.position);
	if(playerDistance <= alertDistance )
	{
	 	if (sensorIn == false)
		{
			alerted = true;
		}
	}

	theAnimator.SetBool("IsTurning", turning);
	
	/*
	if(Input.GetButton("Fire1") && alerted == false)
	{
		alerted = true;
	}	
	*/
	distance = Vector3.Distance(target.position, transform.position);
	if(distance <= awareRange )
	{	
		if (sensorIn == false)
		{
			alerted = true;
		}
	}
	
	
	if(alerted)
	{
		//patrolling behaviour
		targetedPt = target; // chase after the target not the next patroll point
		
	
		//TurnToPlayer(); // change to TurnToPoint()
		TurnToPoint();
		
		//zombie attacking
		if(playerDistance <=attackRange && !attacking)
		{
			Attack();
			alertTimer = 0.0;
		}else if (playerDistance > alertDistance && sensorIn == false) //I added the sensor false statement
		{
		
			
			alertTimer += Time.deltaTime;
			if(alertTimer > alertTimeOut)
			{
				alerted = false;
			}
		}
		
		//zombie attack  false and true menagement
		if(attacking && attackEnabled)
		{
			attackTimer -= Time.deltaTime;
			if(attackTimer <= 0.0)
			{
				playerDamaged = false;
				attacking = false;
			}
			if(attackTimer < 1.2 && playerDistance <= attackRange && !playerDamaged)
			{
				DamagePlayer();
			}
		}
		
		if(angle > 5 ||angle < -5)
		{
			turning = true;
			theAnimator.SetFloat("Speed",0.0);
		}
		else if(angle < 5 && angle > -5)
		{
			if(soundReady)
			{
				Snarl();
			}
			else
			{
				turning = false;
				WalkTowards();
			}
		}
	}else{
		//when the zombie is changeing direction in witch is he walking,
		// 1) on geting to the patrol Pt Distance
		// 2) when hes Alert Timer i biger the the alertTimeOut = he run after the player but he menaged to escape, so the zombie i turning to next patroling point
		if(patrolPtDistance<= changeDistance || alertTimer>=alertTimeOut)
		{
			// zero out the timer from chaseing the player 
			alertTimer = 0.0;
			
			ChangePt();
			turning = true;
		}
	}
	if(turning)
	{
		TurnToPoint();
		
		if(angle<2 && angle > -2)
		{
			WalkTowards();
		}
	}
	if(currPt> patrolPts.Length-1)
	{
		currPt = 0;
	}
	
}


function ChangePt()
{
	currPt++;
}

function Walks()
{
	theAnimator.SetBool("IsWalking",true);
	return;
}

function TurnToPoint()
{
	theAnimator.SetBool("IsWalking",false);
	//var localRotate = transform.InverseTransformPoint(target.position);
	var localRotate = transform.InverseTransformPoint(targetedPt.position); // targetPt not target
	angle = Mathf.Atan2 (localRotate.x, localRotate.z) * Mathf.Rad2Deg;
	var maxRotation = turnSpeed * Time.deltaTime;
	var turnAngle = Mathf.Clamp(angle, -maxRotation, maxRotation);
	transform.Rotate(0, turnAngle, 0);
	return angle;
}

function WalkTowards()
{
	turning = false; // add form patroll behaviour
	//var direction = transform.TransformDirection(Vector3.forward * 	walkSpeed);
	var direction = transform.TransformDirection(Vector3.forward * 	speed); // version with adjusted walking animation cycle
	charControl.SimpleMove(direction);
	theAnimator.SetBool("IsWalking", true);
}

function Snarl()
{
	GetComponent.<AudioSource>().PlayOneShot(snarlSound);
	
	theAnimator.SetTrigger("IsSnarling");
	soundReady = false;
}

function Attack()
{
	theAnimator.SetTrigger("IsAttacking");
	attackTimer = attackDuration;
	
	attacking = true;
	
	//add demage to player OLD version
	//var playerStatus = target.Find("VIKING");
	//playerStatus.SendMessage("AddDamage",damage);
	
	//add demage to player New version
	
}

function DamagePlayer()
{
	playerDamaged = true;
	//var playerStatus = target.Find("VIKING");
	var playerStatus = GameObject.Find("VIKING");
	var LifeBar = GameObject.FindWithTag("FlameLife");
	
	if (playerStatus != null)
	{
		Debug.Log("Player Status ma jakąś wartość różną od null");
	} else{
		Debug.Log(" null");
	
	}
	Debug.Log("DemagePlayer function starts");
	//yield WaitForSeconds(demageWait);
	Debug.Log("DemagePlayer function ends");
	playerStatus.SendMessage("AddDamage" ,  damage);
	LifeBar.SendMessage("AddDemage", damageVoid);
	
}


//demage the zombie
function AddDamage(damage:int)
{
	zombieHealth -= damage;
	if(zombieHealth <= 0)
	{
		zombieHealth = 0;
		Dead();
	}
	
}

function DeactivateAI()
{
	attackEnabled = false;
}

// zombie is killed ADD a RAGDOLL prefab *****************************
function Dead()
{
	Destroy(gameObject);
	if(deadPrefab)
	{
		var dead : Transform = Instantiate(deadPrefab, transform.position, transform.rotation);
		CopyTransformsRecurse(transform, dead);
	}
}
// this function gets the original Enemy coordinates and aplies it to the instanced ragdoll elements
static function CopyTransformsRecurse (src:Transform, dst:Transform)
{
	dst.position = src.position;
	dst.rotation =src.rotation;
	
	for(var child : Transform in dst)
	{
		var curSrc = src.Find(child.name);
		if(curSrc)
			CopyTransformsRecurse(curSrc, child);
	}
}