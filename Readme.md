**About**

Repository contains files used in several projects.

-Viking Mecanim (2014/2015)- scripted animator and controller on a self made character rig, viking model and animations for him.

-Void (2015, update 2016) - interactive experience inspired by self development literature. (Extra elements: story, UI/UX, scripted VFXs, cinematics on in game camera etc.) (Zombie AI added in 2016)

-Valhalla: Viking vs Zombies (2015/2016) - You were killed in a battle. Game is an uncommon interpretation about nordic underworld, on which gates we start this experience. (Extra elements, new story and UI/UX, animated particle emiters, Zombie AI monster, collectibles, destructibles etc.)

Used Tools: Unity, C#, Unity Mecanim and Animation systems, 3ds Max {self made character rig, viking model and animations for him}

**Scripts Locations**

Viking Mecanim:

Assets/Viking Mecanim/Scripts/VikingController.cs

Assets/Viking Mecanim/Scripts/CameraTPP.cs

Void:

Assets/VOID_Game/Scripts/*.cs

Story Telling:

Assets/StoryTelling/Napisy.cs

Assets/StoryTelling/StoryTelling.cs

Assets/StoryTelling/Text_writing.cs

Valhalla: 

Assets/VikingVsZombies4.6/Scripts/...

**Games builds -> downloads**

Valhalla-Pc.rar -

Void-Pc.rar -

Downloads: https://drive.google.com/drive/folders/1OFgLguNiBRYJ0C1Jt601t6lVbOTCRbUF?usp=sharing

**Viking Mecanim and Void movie presentations**

http://www.boldbeargames.com/games/

(on compilation from 2015)

0:00-1:22
https://www.youtube.com/watch?v=DF_c5rAk1wA

(on compilation from 2017)

1:49-1:56
https://youtu.be/d_acAfntVlw?t=109

1:14-1:21
https://youtu.be/d_acAfntVlw?t=74

**Viking Mecanim and Void - Images**

https://drive.google.com/drive/folders/1LKC3E9Lx7s3beffJxiv30qq-3aAr0q1y?usp=sharing


**Valhalla: Viking vs Zombies - Movie Presentation**

http://www.boldbeargames.com/games/

(on compilation from 2017)

0:58-1:06
https://youtu.be/d_acAfntVlw?t=58

2:37-2:43
https://youtu.be/d_acAfntVlw?t=157
